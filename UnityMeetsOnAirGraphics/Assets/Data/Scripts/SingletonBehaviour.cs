﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Braver
{
    public class SingletonBehaviour<T> : MonoBehaviour where T : SingletonBehaviour<T>
    {
        private static T INSTANCE;

        public static T Instance
        {
            get
            {
                if (INSTANCE == null)
                {
                    INSTANCE = FindObjectOfType<T>();
                }

                return INSTANCE;
            }
        }
    }
}


