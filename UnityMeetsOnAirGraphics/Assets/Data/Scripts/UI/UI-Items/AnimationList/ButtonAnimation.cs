﻿using System.Collections;
using System.Collections.Generic;
using Braver.Domain;
using Braver.Overlay;
using DG.Tweening;
using UnityEngine;

namespace Braver.UI
{
    public class ButtonAnimation : MonoBehaviour
    {
        [SerializeField]
        private List<ItemAnimation> animationsToExecute = new List<ItemAnimation>();

        private Transform overlayItemToAnimate;

        public void InitButtonAnimation(Transform itemToAnimate,ItemAnimation itemAnimation)
        {
            overlayItemToAnimate = itemToAnimate;
            animationsToExecute.Add(itemAnimation);
        }

        public void AddAnimationsOnCurrentOverlayItem(ItemAnimation itemAnimation)
        {
            animationsToExecute.Add(itemAnimation);
        }

        public void ExecuteAnimations()
        {
            if (animationsToExecute.Count <= 0 || overlayItemToAnimate == null)
            {
                return;
            }
            foreach (var itemAnimation in animationsToExecute)
            {
                DOTween.Rewind(itemAnimation.AnimId);
                AnimationController.AnimatePosition(itemAnimation,overlayItemToAnimate);
            }
        }

    }
}

