﻿using System.Collections;
using System.Collections.Generic;
using Braver.Domain;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Braver.UI
{
    public class AnimationListUI : MonoBehaviour
    {
        [SerializeField]
        private Transform content;

        [SerializeField]
        private GameObject animButtonPrefab;


        public void ResetContent()
        {
            for (int i = 0; i < content.childCount; i++)
            {
                Destroy(content.GetChild(i).gameObject);
            }
        }

        public GameObject CreateAnimButton(ItemAnimation itemAnim, Transform overlayTransform)
        {
            GameObject newAnimButton = Instantiate(animButtonPrefab, content);
            newAnimButton.GetComponent<ButtonAnimation>().InitButtonAnimation(overlayTransform,itemAnim);
            return newAnimButton;
        }
    }
}


