﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Braver.UI
{
    public class ToolbarUI : MonoBehaviour
    {
        [SerializeField]
        private GameObject createModeCanvas = null;
        [SerializeField]
        private GameObject broadcastModeCanvas = null;

        

        private void Awake()
        {
            if(createModeCanvas == null || broadcastModeCanvas == null)
            {
                Debug.LogError("Unable to obtain the canvases for one or both canvas mode, check if correctly assinged in the inspector.", this);
            }
        }

        public void ChangeToBroadcastMode()
        {
            createModeCanvas.SetActive(false);
            broadcastModeCanvas.SetActive(true);
            Braver.Controller.ProgramController.ChangeToBroadcastMode();

        }
        public void ChangeToCreateMode()
        {
            
            broadcastModeCanvas.SetActive(false);
            createModeCanvas.SetActive(true);
            Braver.Controller.ProgramController.ChangeToCreateMode();
        }
    }
    
}