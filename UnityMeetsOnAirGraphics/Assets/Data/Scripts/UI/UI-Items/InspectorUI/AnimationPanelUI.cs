﻿using System;
using System.Collections;
using System.Collections.Generic;
using Braver.Controller;
using Braver.Domain;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Braver.UI
{
    public class AnimationPanelUI : MonoBehaviour
    {
        [SerializeField]
        private GameObject animationPrefab = null;

        [SerializeField]
        private TMP_Dropdown templateDropdown;

        private InspectorController inspectorController;

        private void Awake()
        {
            if (animationPrefab == null)
            {
                Debug.LogError("No Animation prefab associated.",this);
            }

            inspectorController = GetComponentInParent<InspectorController>();

            List<AnimationTemplate> animationTemplates = ProgramController.Instance.ObtainAnimationTemplates();
            templateDropdown.options.Clear();
            foreach (var animTemp in animationTemplates)
            {
                templateDropdown.options.Add(new TMP_Dropdown.OptionData(animTemp.name));
            }
        }

        public void LoadAnimationsPanel(List<ItemAnimation> animationsList)
        {
            foreach (var itemAnimation in animationsList)
            {
                InitializeAnimationGameObject(itemAnimation);
            }
        }

        public void CreateNewAnimationPanel(ItemAnimation anim)
        {
            InitializeAnimationGameObject(anim);
        }

        private void InitializeAnimationGameObject(ItemAnimation anim)
        {
            GameObject animGameObject = Instantiate(animationPrefab, this.transform);
            animGameObject.GetComponent<InspectorAnim>().itemAnimation = anim;
            SetAnimExecDropdown(animGameObject, anim);

            //From/To
            Transform fromPositionTrans = animGameObject.transform.GetChild(0).GetChild(2);
            Toggle fromPositionToggle = fromPositionTrans.GetComponent<Toggle>();
            fromPositionToggle.isOn = anim.IsFromCurrentPosition();
            fromPositionToggle.onValueChanged.AddListener(anim.ChangeFromCurrentValue);

            //StartValue
            Transform valueTrans = animGameObject.transform.GetChild(0).GetChild(3);
            TMP_InputField[] valueIF = valueTrans.GetComponentsInChildren<TMP_InputField>();
            valueIF[0].text = anim.ObtainValue().x.ToString();
            valueIF[0].onValueChanged.AddListener(x => ChangeValueX(anim, x));
            valueIF[1].text = anim.ObtainValue().y.ToString();
            valueIF[1].onValueChanged.AddListener(x => ChangeValueY(anim, x));
            valueIF[2].text = anim.ObtainValue().z.ToString();
            valueIF[2].onValueChanged.AddListener(x => ChangeValueZ(anim, x));

            //Duration
            Transform durationTrans = animGameObject.transform.GetChild(0).GetChild(4);
            TMP_InputField durationIF = durationTrans.GetComponentInChildren<TMP_InputField>();
            durationIF.text = anim.ObtainDuration().ToString();
            durationIF.onValueChanged.AddListener(x => ChangeDuration(anim, x));
        }
        private void SetAnimExecDropdown(GameObject animGO, ItemAnimation anim)
        {
            TMP_Dropdown animExecDropdowns = animGO.GetComponentInChildren<TMP_Dropdown>();
            animExecDropdowns.ClearOptions();
            foreach (var exec in Enum.GetNames(typeof(AnimExecution)))
            {
                animExecDropdowns.options.Add(new TMP_Dropdown.OptionData(exec));
            }
            animExecDropdowns.value = (int)anim.animExecution;

            animExecDropdowns.onValueChanged.AddListener(x => anim.animExecution = (AnimExecution)x);
        }

        private void ChangeValueX(ItemAnimation itemSelected, string changedValue)
        {
            if (changedValue.Length > 0 || changedValue != "-")
            {
                itemSelected.ChangeValue(new Vector3(float.Parse(changedValue),itemSelected.ObtainValue().y, itemSelected.ObtainValue().z));
            }
        }
        private void ChangeValueY(ItemAnimation itemSelected, string changedValue)
        {
            if (changedValue.Length > 0 || changedValue != "-")
            {
                itemSelected.ChangeValue(new Vector3(itemSelected.ObtainValue().x, float.Parse(changedValue), itemSelected.ObtainValue().z));
            }
        }
        private void ChangeValueZ(ItemAnimation itemSelected, string changedValue)
        {
            if (changedValue.Length > 0 || changedValue != "-")
            {
                itemSelected.ChangeValue(new Vector3(itemSelected.ObtainValue().x, itemSelected.ObtainValue().y, float.Parse(changedValue)));
            }
        }
        private void ChangeDuration(ItemAnimation itemSelected, string changedValue)
        {
            if (changedValue.Length > 0 || changedValue != "-")
            {
                itemSelected.ChangeDuration(float.Parse(changedValue));
            }
        }

        public void CreateNewAnimationFromTemplate()
        {
            AnimationTemplate animTemp = ProgramController.Instance.ObtainAnimationTemplates()[templateDropdown.value];
            ItemAnimation newAnim = inspectorController.AddAnimationToCurrentSelectedItem(animTemp);
            CreateNewAnimationPanel(newAnim);
        }

    }
}
