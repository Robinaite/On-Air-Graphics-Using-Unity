﻿using System.Collections;
using System.Collections.Generic;
using Braver.Controller;
using UnityEngine;
using Braver.Domain;
using TMPro;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Braver.UI
{
    /// <summary>
    /// Manages the item hierarchy UI
    /// </summary>
    public class ItemHierarchy : MonoBehaviour
    {
        [SerializeField] //TODO Needs to be removed from Serialziefield as it should be dynamic, for now its there for debugging purposes.
        private Braver.Domain.Project currentProject = null; 

        [SerializeField]
        private Transform contentObject = null;

        [SerializeField]
        private GameObject itemButtonNormal = null;
        [SerializeField]
        private GameObject itemButtonWithDropdown = null;

        [SerializeField]
        private InspectorController inspectorController = null;

        private void Awake()
        {
            if(itemButtonNormal == null)
            {
                Debug.LogError("No item Button prefab attached to the item hierarchy object in scene.", this);
            }
            if (itemButtonWithDropdown == null)
            {
                Debug.LogError("No item Button with dropdown prefab attached to the item hierarchy object in scene.", this);
            }
            if (contentObject == null)
            {
                Debug.LogError("No content transform attached to the item hierarchy object in scene for referencing.", this);
            }
            if (inspectorController == null)
            {
                Debug.LogError("No inspector view attached to the item hierarchy object in scene for referencing.", this);
            }
        }
        /// <summary>
        /// Sets the current project in the item hierarchy.
        /// </summary>
        /// <param name="project"></param>
        public void SetCurrentProject(Project project)
        {
            currentProject = project;
            OnSceneChange();
        }
        /// <summary>
        /// Updates the item hierarchy with the new scene item content.
        /// </summary>
        public void OnSceneChange()
        {
            //Delete existing items
            for (int i = contentObject.childCount-1; i >=0; i--)
            {
                Destroy(contentObject.GetChild(i).gameObject);
            }

            Scene currentScene = ProjectController.Instance.currentProject.ObtainCurrentActiveScene();
            //Create all UI items
            foreach (Item item in currentScene.ObtainSceneItems())
            {
                if (item.HasChildrenItems())
                {
                    GameObject mainObj = CreateItemWithChildren(item, 1);
                    mainObj.SetActive(true);
                }
                else
                {
                    GameObject mainObj = CreateItemButton(item);
                    mainObj.SetActive(true);
                }
            }
        }
        /// <summary>
        /// Creates the UI Object if the item has child items.
        /// Recursively creates the child items.
        /// </summary>
        /// <param name="item">The item to craete the button for</param>
        /// <param name="identation">Recursive parameter, used to know how many identations</param>
        /// <returns>The created UI Button in the item hierarchy</returns>
        private GameObject CreateItemWithChildren(Item item,int identation)
        {
            GameObject itemButtonCreated = Instantiate(itemButtonWithDropdown, contentObject);
            SetupItemButton(item, itemButtonCreated);
            foreach(Item childItem in item.ObtainChildrenItems())
            {
                GameObject subItemCreated = null;
                if (childItem.HasChildrenItems())
                {
                    subItemCreated = CreateItemWithChildren(childItem,identation+1);
                }
                else
                {
                    subItemCreated = CreateItemButton(childItem);
                }
                if(subItemCreated != null)
                {
                    itemButtonCreated.GetComponent<ItemButtonWithChildren>().AddItemButtonAsChild(subItemCreated,identation); //Necessary for correct view toggling.
                }
            }
            return itemButtonCreated;
        }
        /// <summary>
        /// Creates a normal item button with no child items.
        /// </summary>
        /// <param name="item">the item to create a button for</param>
        /// <returns></returns>
        private GameObject CreateItemButton(Item item)
        {
            GameObject itemButtonCreated = Instantiate(itemButtonNormal, contentObject);
            SetupItemButton(item, itemButtonCreated);
            return itemButtonCreated;
        }
        /// <summary>
        /// Sets up all the components of the item button.
        /// 1. Associates the item button to the item SO where it was created for.
        /// </summary>
        /// <param name="item">The item the button was created for</param>
        /// <param name="go">The item button</param>
        private void SetupItemButton(Item item, GameObject go)
        {
            go.gameObject.name = item.name + item.globalOrder;
            go.GetComponent<ItemButton>().SetItemSO(item);
            go.GetComponentInChildren<TextMeshProUGUI>().text = item.name;

            Button[] buttons = go.GetComponentsInChildren<Button>();
            if (buttons.Length == 2)
            {
                foreach (var button in buttons)
                {
                    if (button.gameObject.CompareTag("sceneButton"))
                    {
                        button.onClick.AddListener(() => LoadItem(item,go));
                    }
                }
            }
            else
            {
                buttons[0].onClick.AddListener(() => LoadItem(item,go));
            }
        }

        public void LoadItem(Item selectedItem,GameObject itemButton)
        {
            inspectorController.LoadItem(selectedItem,itemButton);
        }
    }
}

