﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Braver.Domain;
using TMPro;

namespace Braver.UI
{
    /// <summary>
    /// Manages the sceneToSet button UI.
    /// </summary>
    public class SceneButton : MonoBehaviour
    {
        [SerializeField]
        public Scene scene;

        private SceneList sceneList;

        private void Awake()
        {
            sceneList = GetComponentInParent<SceneList>();
        }

        public void ChangeScene()
        {
            sceneList.ChangeScene(scene);
        }

    }
}


