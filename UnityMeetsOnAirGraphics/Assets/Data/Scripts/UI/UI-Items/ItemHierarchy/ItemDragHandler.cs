﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Braver.Controller;
using Braver.Domain;
using Braver.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemDragHandler : MonoBehaviour, IBeginDragHandler,IDragHandler,IEndDragHandler
{
    public bool dragOnSurfaces = true;

    private GameObject mDraggingIcon;
    private RectTransform mDraggingPlane;
    private Canvas parentCanvas;

    public void OnBeginDrag(PointerEventData eventData)
    {
        parentCanvas = GetComponentInParent<Canvas>();
        if (parentCanvas == null)
            return;

        // We have clicked something that can be dragged.
        // What we want to do is create an icon for this.
        mDraggingIcon = new GameObject("icon");

        mDraggingIcon.transform.SetParent(parentCanvas.transform, false);
        mDraggingIcon.transform.SetAsLastSibling();

        var image = mDraggingIcon.AddComponent<Image>();

        image.sprite = GetComponent<Image>().sprite;
        image.SetNativeSize();

        if (dragOnSurfaces)
            mDraggingPlane = transform as RectTransform;
        else
            mDraggingPlane = parentCanvas.transform as RectTransform;

        SetDraggedPosition(eventData);
    }

    public void OnDrag(PointerEventData data)
    {
        if (mDraggingIcon != null)
            SetDraggedPosition(data);
    }

    private void SetDraggedPosition(PointerEventData data)
    {
        if (dragOnSurfaces && data.pointerEnter != null && data.pointerEnter.transform as RectTransform != null)
            mDraggingPlane = data.pointerEnter.transform as RectTransform;

        var rt = mDraggingIcon.GetComponent<RectTransform>();
        Vector3 globalMousePos;
        if (RectTransformUtility.ScreenPointToWorldPointInRectangle(mDraggingPlane, data.position, data.pressEventCamera, out globalMousePos))
        {
            rt.position = globalMousePos;
            rt.rotation = mDraggingPlane.rotation;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (mDraggingIcon != null)
            Destroy(mDraggingIcon);

        ItemDropped();
    }

    private void ItemDropped()
    {
        Vector2 mousePosition = Input.mousePosition;
        //Raycast stuff.
        var raycastResults = RaycastCurrentMousePosition();

        if (raycastResults.Last().gameObject.CompareTag("ItemHierarchy"))
        {
            if (raycastResults.Count > 1)
            {

                GameObject itemButtonHovered = raycastResults.Find(x => x.gameObject.CompareTag("itemButton")).gameObject;
                if (CheckIfSameItemButton(itemButtonHovered)) return;

                
                Item thisItem = this.GetComponent<ItemButton>().itemSO;
                var itemButton = itemButtonHovered.GetComponent<ItemButton>();
                Item itemHovered = itemButton.itemSO;

                //Check if its not parent going into one if its children
                if (CheckIfHoveredItemIsChildOfThis(itemHovered)) return;

                var buttonHeightBy4 = itemButtonHovered.GetComponent<RectTransform>().rect.height / 4;

                var position = itemButtonHovered.transform.position;

                if (position.y + buttonHeightBy4 < mousePosition.y)
                {
                    //Change item to above item in hierarchy
                    Debug.Log("Above");
                    AddItemAboveOrBelowHovered(thisItem, itemHovered,true);
                }
                else if (position.y - buttonHeightBy4 > mousePosition.y)
                {
                    //Change Item to below item in hierarchy
                    Debug.Log("Below");
                    AddItemAboveOrBelowHovered(thisItem, itemHovered, false);
                }
                else
                {
                    //Child the item to the item hovered.
                    Debug.Log("AddChild");
                    ProjectController projectController = ProjectController.Instance;
                    if (projectController.RemoveItemFromScene(thisItem))
                    {
                        itemHovered.AddChildrenItem(thisItem);
                        Scene currentScene = projectController.currentProject.ObtainCurrentActiveScene();
                        currentScene.IncreaseOrderItem(thisItem, false);
                    }
                }

                Debug.Log(mousePosition);
            }
            else
            {
                //Adds the item to the end of the item list 
                Item thisItem = this.GetComponent<ItemButton>().itemSO;
                if (ProjectController.Instance.RemoveItemFromScene(thisItem))
                {
                    ProjectController.Instance.AddItemToCurrentScene(thisItem);
                }
            }

            ProjectController.Instance.ObtainItemHierarchy().OnSceneChange();
            ProjectController.Instance.ObtainOverlayController().UpdateSortingOrder();
        }

        //do nothing
    }
    /// <summary>
    /// Adds the item above or below the hovered item.
    /// </summary>
    /// <param name="thisItem">The current item that is being dragged</param>
    /// <param name="itemHovered">The item being hovered upon</param>
    /// <param name="above">If true, it adds the item above, if false it adds it below the hovered item.</param>
    private static void AddItemAboveOrBelowHovered(Item thisItem, Item itemHovered,bool above)
    {
        ProjectController projectController = ProjectController.Instance;
        Scene currentScene = projectController.currentProject.ObtainCurrentActiveScene();
        if (ProjectController.Instance.RemoveItemFromScene(thisItem))
        {
            int itemHoveredOrder = itemHovered.globalOrder;
            Item fatherItem = itemHovered.fatherItem;
            if (fatherItem != null) //
            {
                fatherItem.AddChildrenItem(thisItem);
            }
            else
            {
                projectController.AddItemToCurrentScene(thisItem);
            }

            currentScene.IncreaseOrderItem(itemHovered, above);
            thisItem.globalOrder = itemHoveredOrder;
            currentScene.SortLists();
        }
    }
    /// <summary>
    /// Checks if hovered item is a child of this item that is dragged.
    /// </summary>
    /// <param name="itemHovered">The item being hovered upon</param>
    /// <param name="thisItem">The current item being dragged</param>
    /// <returns>True if this item is a child of the dragged item, false if not.</returns>
    private bool CheckIfHoveredItemIsChildOfThis(Item itemHovered)
    {
        Item thisItem = this.GetComponent<ItemButton>().itemSO;
        Item itemChecked = itemHovered.fatherItem;
        while (itemChecked != null)
        {
            if (itemChecked.Equals(thisItem))
            {
                Debug.Log("Tried to add parent to child.");
                return true;
            }

            itemChecked = itemChecked.fatherItem;
        }
        return false;
    }
    /// <summary>
    /// Checks if the hovered item is the same as this item which is dragged.
    /// </summary>
    /// <param name="itemButtonHovered">The item hovered upon</param>
    /// <returns>True if same button, false if not</returns>
    private bool CheckIfSameItemButton(GameObject itemButtonHovered)
    {
        if (itemButtonHovered.Equals(this.gameObject))
        {
            Debug.Log("SameObject");
            return true;
        }

        return false;
    }
    /// <summary>
    /// Raycast the current mouse position and returns the result list.
    /// </summary>
    /// <returns></returns>
    private List<RaycastResult> RaycastCurrentMousePosition()
    {
        PointerEventData pointerEventData = new PointerEventData(EventSystem.current);
        pointerEventData.position = Input.mousePosition;

        List<RaycastResult> raycastResults = new List<RaycastResult>();
        var canvasRaycaster = parentCanvas.GetComponent<GraphicRaycaster>();
        canvasRaycaster.Raycast(pointerEventData, raycastResults);
        return raycastResults;
    }
}
