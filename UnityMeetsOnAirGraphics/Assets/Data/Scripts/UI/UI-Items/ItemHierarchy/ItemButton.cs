﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Braver.Domain;
using TMPro;

namespace Braver.UI
{
    /// <summary>
    /// Manages the item button UI.
    /// </summary>
    public class ItemButton : MonoBehaviour
    {
        private static readonly int SIZE_OF_IDENTATION = 10;
        private static readonly int DEFAULT_MARGIN = 17;

        [SerializeField]
        public Item itemSO;

        [SerializeField]
        protected int numIdentations = 0;

        private Vector4 defaultMargin = new Vector4(DEFAULT_MARGIN, 1,0,1);
        /// <summary>
        /// Sets the item identation depending on how deep in the tree it exists.
        /// </summary>
        /// <param name="numIdentationsToChange">How many identations deep is needed.</param>
        public void ChangeItemIdentation(int numIdentationsToChange)
        {
            numIdentations = numIdentationsToChange;
            this.GetComponentInChildren<TextMeshProUGUI>().margin = defaultMargin + new Vector4(numIdentations * SIZE_OF_IDENTATION, 0);
        }
        /// <summary>
        /// Sets the item SO to this Item Button
        /// </summary>
        /// <param name="item">Item SO</param>
        public void SetItemSO(Item item)
        {
            itemSO = item;
        }

        public void LoadItemInspector()
        {

        }


    }
}


