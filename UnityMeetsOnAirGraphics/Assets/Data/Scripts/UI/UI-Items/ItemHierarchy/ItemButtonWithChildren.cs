﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Braver.Domain;

namespace Braver.UI
{
    /// <summary>
    /// Extension of ItemButton, includes all the information for the buttons with dropdowns/child items.
    /// </summary>
    public class ItemButtonWithChildren : ItemButton
    {
        [SerializeField]
        private List<GameObject> childrenItemButtons = new List<GameObject>();
        /// <summary>
        /// Toggles the children items visibility.
        /// </summary>
        public void ToggleChildrenItemsButtons()
        {
            foreach(GameObject itemButton in childrenItemButtons)
            {
                if (itemButton.activeInHierarchy)
                {
                    ItemButtonWithChildren itemButtonWithChildren = itemButton.GetComponent<ItemButtonWithChildren>();
                    if (itemButtonWithChildren != null)
                    {
                        itemButtonWithChildren.DisableChildrenItemButtons(); //In case a higher-up item toggles itself, hide all children and sub-children.
                    }
                    itemButton.SetActive(false); //Hide the children items
                } else
                {
                    itemButton.SetActive(true); //Show the children items
                }
            }
        }

        /// <summary>
        /// Disables and removes the visibility of the children items.
        /// </summary>
        public void DisableChildrenItemButtons()
        {
            foreach (GameObject itemButton in childrenItemButtons)
            {
                itemButton.SetActive(false); //Hide the children items
            }
        }
        /// <summary>
        /// Adds the item button as a child of this button.
        /// </summary>
        /// <param name="itemButton">The item button to add</param>
        /// <param name="identation">How many identations is needed for this child</param>
        public void AddItemButtonAsChild(GameObject itemButton,int identation)
        {
            itemButton.GetComponent<ItemButton>().ChangeItemIdentation(identation);
            childrenItemButtons.Add(itemButton);
        }
        /// <summary>
        /// Removes item button from child list.
        /// </summary>
        /// <param name="itemButton"></param>
        public void RemoveItemButtonFromChild(GameObject itemButton)
        {
            childrenItemButtons.Remove(itemButton);
        }
    }
}


