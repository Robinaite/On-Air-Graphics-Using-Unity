﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TopMenuSubMenuItemsUI : MonoBehaviour, IPointerExitHandler
{
    [SerializeField]
    private GameObject subMenuItems = null;

    [SerializeField]
    private GameObject subSubMenuProjects = null;
    /// <summary>
    /// When the pointer leaves the sub-menu disables the sub-menu items.
    /// </summary>
    /// <param name="eventData">The pointer event data</param>
    public void OnPointerExit(PointerEventData eventData)
    {
        subMenuItems.SetActive(false);
        if (subSubMenuProjects != null)
        {
            subSubMenuProjects.SetActive(false);
        }
    }

    /// <summary>
    /// Method used when clicked on the button to show the subMenuItems.
    /// </summary>
    public void ShowSubMenuItems()
    {
        subMenuItems.SetActive(true);
    }

}
