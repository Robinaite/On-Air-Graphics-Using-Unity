﻿namespace Braver.UI
{
    /// <summary>
    /// All the different UI Spaces in the product.
    /// </summary>
    public enum UISpaces { ItemHierarchy, SceneHierarchy, OverlayWindow, otherPlace }
}

