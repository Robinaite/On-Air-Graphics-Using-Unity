﻿using System.Collections;
using System.Collections.Generic;
using Braver.Controller;
using Braver.Domain;
using Braver.Overlay;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Braver.UI
{
    public class SceneList : MonoBehaviour
    {
        [SerializeField]
        private Transform contentObject = null;

        [SerializeField]
        private ItemHierarchy itemHierarchy = null;

        private ProjectController projectController = null;

        [SerializeField]
        private GameObject sceneButton = null;

        private ContextMenu contextMenu;

        private GraphicRaycaster canvasRaycaster;
        private EventSystem eventSystem;
        private static readonly string SCENE_BUTTON_TAG = "sceneButton";

        private void Awake()
        {
            projectController = ProjectController.Instance;
            if (sceneButton == null)
            {
                Debug.LogError("No scene Button prefab attached to theScene List UI object in scene.", this);
            }
            if (contentObject == null)
            {
                Debug.LogError("No content transform attached to the Scene List UI object in scene for referencing.", this);
            }
            if (itemHierarchy == null)
            {
                Debug.LogError("There's no itemHierarchy script associated to the Scene List!", this);
            }
            if (projectController == null)
            {
                Debug.LogError("There's no project controller script associated to the Scene List!", this);
            }
            canvasRaycaster = GetComponentInParent<GraphicRaycaster>();
            if (canvasRaycaster == null)
            {
                Debug.LogError("There is no GraphicRaycaster attached to the main canvas game object.", this);
            }
            eventSystem = EventSystem.current;
            if (eventSystem == null)
            {
                Debug.LogError("There is no Event System in this main scene.", this);
            }

            contextMenu = GetComponentInParent<ContextMenu>();
            if (contextMenu == null)
            {
                Debug.LogError("There is no context menu script in this canvas.", this);
            }
        }
        /// <summary>
        /// Loads the scene list from a project model item.
        /// </summary>
        /// <param name="project">The newly loaded/selected project.</param>
        public void LoadProject(Project project)
        {
            for (int i = 0; i < contentObject.childCount; i++)
            {
                Destroy(contentObject.GetChild(i).gameObject);
            }

            foreach (var scene in project.ObtainSceneList())
            {
                GameObject sceneButtonClone = Instantiate(sceneButton, contentObject);
                sceneButtonClone.GetComponent<SceneButton>().scene = scene;
                if (scene.Equals(project.ObtainCurrentActiveScene()))
                {
                    sceneButtonClone.GetComponent<Button>().Select();
                }
            }
            itemHierarchy.SetCurrentProject(project);
        }
        /// <summary>
        /// Creates a new scene in the scene list and notifies the item hierarchy that the active scene could have changed. 
        /// </summary>
        /// <param name="sceneCreated">The scene model item to base it on.</param>
        public void CreateNewScene(Scene sceneCreated)
        {
            //Create the respective object in the 
            GameObject sceneButtonClone = Instantiate(sceneButton, contentObject);
            sceneButtonClone.GetComponent<SceneButton>().scene = sceneCreated;
            sceneButtonClone.GetComponent<Button>().Select();
            itemHierarchy.OnSceneChange();
        }

        public void DeleteSelectedScene()
        {
            PointerEventData pointerEventData = new PointerEventData(eventSystem);
            pointerEventData.position = contextMenu.ContextMenuOpenedMousePosition;

            List<RaycastResult> raycastResults = new List<RaycastResult>();

            canvasRaycaster.Raycast(pointerEventData, raycastResults);

            int raycastObjectIndex = -1;
            if (raycastResults[0].gameObject.tag == SCENE_BUTTON_TAG)
            {
                raycastObjectIndex = 0;
                
            } else if (raycastResults[1].gameObject.tag == SCENE_BUTTON_TAG)
            {
                raycastObjectIndex = 1;
            }

            if (raycastObjectIndex == 0 || raycastObjectIndex == 1)
            {
                projectController.DeleteScene(raycastResults[raycastObjectIndex].gameObject.GetComponent<SceneButton>().scene);
            }
        }

        public void ChangeScene(Scene selectedScene)
        {
            projectController.ChangeScene(selectedScene);
            OverlayController.Instance.ChangeToScene(selectedScene);
            itemHierarchy.OnSceneChange();
        }

    }
}

