﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Braver.UI
{
    /// <summary>
    /// This class is to join together which UI Options can be used depending on which place you clicked.
    /// </summary>
    [System.Serializable]
    public class Option : System.Object
    {
        [SerializeField]
        private GameObject option = null;
        [SerializeField]
        private List<UISpaces> spacesOptionAvailable = new List<UISpaces>();
        /// <summary>
        /// Check if the option is allowed to appear on the clicked space.
        /// </summary>
        /// <param name="uiSpace">The UI space value of where the user clicked.</param>
        /// <returns>True, if allowed to show, false, if not allowed.</returns>
        public bool ContaisSpace(UISpaces uiSpace)
        {
            return spacesOptionAvailable.Contains(uiSpace);
        }
        /// <summary>
        /// Sets the respective gameobject active status.
        /// </summary>
        /// <param name="value">Active status to change too.</param>
        public void SetOptionActive(bool value)
        {
            if (option == null)
            {
                Debug.LogError("Option doesn't have a Game Object associated. Check the Context Menu Options script attached to the main context menu game object in the scene.");
                return;
            } 
            option.SetActive(value);
        }
    }
}
