﻿using System;
using System.Collections;
using System.Collections.Generic;
using Braver.Controller;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
namespace Braver.UI
{
    /// <summary>
    /// Manages the context menu pop-up behaviour.
    /// </summary>
    public class ContextMenu : MonoBehaviour
    {
        #region Static Variables
        private static readonly string CONTEXT_MENU_TAG = "contextMenu";
        #endregion


        [SerializeField]
        private GameObject contextMenuUI = null;
        private RectTransform mainOptionsRT;

        [SerializeField]
        private UISpaces clickedUISpace = UISpaces.otherPlace;

        private GraphicRaycaster canvasRaycaster;
        private EventSystem eventSystem;

        public Vector3 ContextMenuOpenedMousePosition { get; private set; }

        private void Start()
        {
            if(contextMenuUI == null)
            {
                Debug.LogError("There is no Context Menu Game Object associated to this canvas.",this);
            } else
            {
                mainOptionsRT = contextMenuUI.GetComponent<RectTransform>();
            }
            canvasRaycaster = GetComponent<GraphicRaycaster>();
            if(canvasRaycaster == null)
            {
                Debug.LogError("There is no GraphicRaycaster attached to the main canvas game object.", this);
            }
            eventSystem = EventSystem.current;
            if(eventSystem == null)
            {
                Debug.LogError("There is no Event System in this main scene.", this);
            }
            
        }

        private void Update()
        {
            if (Input.GetMouseButtonUp(1))
            {
                RightClick();
            }
            else if (Input.GetMouseButtonDown(0))
            {
                LeftClick();
            }
        }
        /// <summary>
        /// Manages the context menu actions when a left click happens.
        /// </summary>
        private void LeftClick()
        {
            if (contextMenuUI.activeInHierarchy)
            {
                PointerEventData pointerEventData = new PointerEventData(eventSystem);
                pointerEventData.position = Input.mousePosition;

                List<RaycastResult> raycastResults = new List<RaycastResult>();

                canvasRaycaster.Raycast(pointerEventData, raycastResults);

                #region Debugging Raycasts
                //Debug.Log(raycastResults.Count);
                //if (raycastResults.Count != 0)
                //{
                //    foreach (RaycastResult result in raycastResults)
                //    {
                //        Debug.Log(result.gameObject.tag);
                //    }
                //}
                #endregion

                if (raycastResults.Count == 0)
                {
                    contextMenuUI.SetActive(false);
                }
                // The first item of a raycast will always be the UI on-top which in this case should be the Context Menu at all moments
                else if (raycastResults[0].gameObject.tag != CONTEXT_MENU_TAG)
                {
                    contextMenuUI.SetActive(false);
                }
            }
        }
        /// <summary>
        /// Manages the context menu actions when a right click happens.
        /// We do a disable/enable, to reset the context menu sub-items so they receive an OnEnable command when enabled again.
        /// </summary>
        private void RightClick()
        {
            if (ProgramController.IsInCreateMode())
            {
                contextMenuUI.SetActive(false);
                SetClickedUISpace();
                SetContextMenuPosition();
                contextMenuUI.SetActive(true);
            }
        }
        /// <summary>
        /// Checks where the right click happened and changes the Pivot of the menu respectively to have the menu always show on screen.
        /// </summary>
        private void SetContextMenuPosition()
        {
            Vector3 currentMousePosition = Input.mousePosition;
            ContextMenuOpenedMousePosition = currentMousePosition;
            if (currentMousePosition.y <= Screen.height / 2)
            {
                mainOptionsRT.pivot = new Vector2(mainOptionsRT.pivot.x, -0.05f);
            }
            else
            {
                mainOptionsRT.pivot = new Vector2(mainOptionsRT.pivot.x, 1f);
            }
            if (currentMousePosition.x <= Screen.width * 3 / 4)
            {
                mainOptionsRT.pivot = new Vector2(-0.05f, mainOptionsRT.pivot.y);
            }
            else
            {
                mainOptionsRT.pivot = new Vector2(1f, mainOptionsRT.pivot.y);
            }
            contextMenuUI.transform.position = Input.mousePosition;
        }
        /// <summary>
        /// Raycasts to obtain which UI panel was clicked.
        /// </summary>
        private void SetClickedUISpace()
        {
                PointerEventData pointerEventData = new PointerEventData(eventSystem);
                pointerEventData.position = Input.mousePosition;

                List<RaycastResult> raycastResults = new List<RaycastResult>();

                canvasRaycaster.Raycast(pointerEventData, raycastResults);
                string clickedObjectTag = "otherplace";
                if (raycastResults.Count > 0)
                {
                    //The last one because the panel should always be the one which is behind everything.
                    clickedObjectTag = raycastResults[raycastResults.Count - 1].gameObject.tag;
                }

                switch (clickedObjectTag)
                {
                    case "ItemHierarchy":
                        clickedUISpace = UISpaces.ItemHierarchy;
                        break;
                    case "SceneHierarchy":
                        clickedUISpace = UISpaces.SceneHierarchy;
                        break;
                    case "OverlayWindow":
                        clickedUISpace = UISpaces.OverlayWindow;
                        break;
                    default:
                        clickedUISpace = UISpaces.otherPlace;
                        break;
                }
        }
        /// <summary>
        /// Obtains the last clicked UI Space.
        /// </summary>
        /// <returns>The UISpace Clicked</returns>
        public UISpaces GetClickedSpace()
        {
            return clickedUISpace;
        }
    }
}
