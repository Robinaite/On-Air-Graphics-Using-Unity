﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Braver.UI {
    /// <summary>
    /// Handles the existence of the options in the menu.
    /// </summary>
    public class ContextMenuOptions : MonoBehaviour
    {
        [SerializeField]
        private List<Option> optionsList = new List<Option>();

        private ContextMenu contextMenu = null;

        private void Awake()
        {
            contextMenu = GetComponentInParent<ContextMenu>();
            if(contextMenu == null)
            {
                Debug.LogError("Unable to find the main canvas, in a parent GameObject, with the contextMenu Manager script.", this);
            }
        }
        /// <summary>
        /// When this game object gets enabled, it checks, taking into account where the click happened, all the options that are allowed to be shown.
        /// </summary>
        private void OnEnable()
        {
            foreach(Option option in optionsList)
            {
                if (option.ContaisSpace(contextMenu.GetClickedSpace()))
                {
                    option.SetOptionActive(true);
                } else
                {
                    option.SetOptionActive(false);
                }
            }
        }

    }
}
