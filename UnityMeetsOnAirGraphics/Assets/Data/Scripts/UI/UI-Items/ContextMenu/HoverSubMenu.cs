﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
namespace Braver.UI
{
    /// <summary>
    /// Manages the sub-context menus if exists.
    /// </summary>
    public class HoverSubMenu : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField]
        private GameObject subMenu = null;

        private RectTransform subMenuRT;

        private void Awake()
        {
            if(subMenu == null)
            {
                Debug.LogError("No SubMenu Game Object found to be shown when hovering over this item.", this);
            }
            subMenuRT = subMenu.GetComponent<RectTransform>();
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            SetSubMenuPosition(eventData);
            subMenu.SetActive(true);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            subMenu.SetActive(false);
        }
        /// <summary>
        /// Checks where the right click happened and changes the Pivot of the sub-menu respectively to have the submenu always show on screen.
        /// </summary>
        /// <param name="eventData">The mouse event data</param>
        private void SetSubMenuPosition(PointerEventData eventData)
        {
            Vector3 currentMousePosition = eventData.position;
            
            if (currentMousePosition.y <= Screen.height / 2)
            {
                subMenuRT.pivot = new Vector2(subMenuRT.pivot.x, 0.50f); //Could need change depending on how many buttons in context menu. 
            }
            else
            {
                subMenuRT.pivot = new Vector2(subMenuRT.pivot.x, 1f);
            }
            if (currentMousePosition.x <= Screen.width * 3 / 4)
            {
                subMenuRT.pivot = new Vector2(0, subMenuRT.pivot.y);
            }
            else
            {
                subMenuRT.pivot = new Vector2(2f, subMenuRT.pivot.y);
            }
        }
    }
}