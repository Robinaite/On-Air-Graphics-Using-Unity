﻿using System;
using System.Collections;
using System.Collections.Generic;
using Braver.Controller;
using Braver.Domain;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ProjectListUI : MonoBehaviour
{
    [SerializeField]
    private GameObject projectButton = null;
    [SerializeField]
    private Transform content = null;

    [SerializeField]
    private GameObject itemsToHide = null;

    private ProjectController projectController = null;

    private void Awake()
    {
        projectController = ProjectController.Instance;
        if (projectButton == null)
        {
            Debug.LogError("Project Button prefab missing. Add it to this object.",this);
        }

        if (content == null)
        {
            Debug.LogError("Contet object missing. select where the project buttons are added to.",this);
        }
    }

    public void createProjectButtonsList()
    {
            for (int i = 0; i < content.childCount; i++)
            {
                Destroy(content.GetChild(i).gameObject);
            }

            foreach (String projectName in ProgramController.ObtainSavedProjectList())
            {
                GameObject prButton = Instantiate(projectButton, content);

                TextMeshProUGUI buttonText = prButton.GetComponentInChildren<TextMeshProUGUI>();
                buttonText.text = projectName;
                Button button = prButton.GetComponent<Button>();
                button.onClick.AddListener(() => loadProject(projectName));
                button.onClick.AddListener(() =>
                {
                    content.gameObject.SetActive(false);
                    itemsToHide.gameObject.SetActive(false);
                });
            }
    }

    private void loadProject(string projectToLoad)
    {
        projectController.SaveCurrentProject();
        var project = LoadProjectController.LoadProject(projectToLoad);
        projectController.ChangeProject(project);
    }

}
