﻿using UnityEngine;
using Braver.Domain;
using SimpleFileBrowser;
using System.Collections;
using Braver.Overlay;

namespace Braver.Controller
{
    public class CreateItemsController : MonoBehaviour
    {
        private ProjectController projectController = null;

        private Overlay.OverlayController overlayController = null;

        private Braver.UI.ItemHierarchy itemHierarchy = null;

        private static string FILE_PREFIX = "file:///";

        private void Awake()
        {
            projectController = ProjectController.Instance;
            if(projectController == null)
            {
                Debug.LogError("Unable to access the project controller in this object, please associate it through the editor to this game Object.", this);
                return;
            }

            overlayController = OverlayController.Instance;
            if(overlayController == null)
            {
                Debug.LogError("Unable to get overlay controller. Check project controller!", this);
            }
            itemHierarchy = projectController.ObtainItemHierarchy();
            if(itemHierarchy == null)
            {
                Debug.LogError("Unable to get Item Hierarchy UI. Check project controller!", this);
            }

            //Setup File Browser Filters
            FileBrowser.SetFilters(true, new FileBrowser.Filter("Images", ".jpg", ".png"));
            FileBrowser.SetDefaultFilter(".jpg");
        }
        /// <summary>
        /// Executes the creaty empty item functionalities
        /// </summary>
        public void CreateEmptyItem()
        {
            //First execute the domain steps
            //Create the empty item in domain space
            Item item = ItemFactory.CreateEmptyItem();
            SetupItem(item);

        }
        /// <summary>
        /// Executes the create image item functions. Continues in the private method with the same name after.
        /// </summary>
        public void CreateImageItem()
        {
            //First open file browser and when image is selected execute private createImageMethod
            FileBrowser.ShowLoadDialog((path) => { CreateImageItem(FILE_PREFIX + path); }, () => { Debug.Log("Canceled"); }, false, null, "Select Image", "Select");
        }
        /// <summary>
        /// Creates the image item based on the file selected
        /// </summary>
        /// <param name="filePath"> path till the file</param>
        private void CreateImageItem(string filePath)
        {
            //Creates the image item in domain space
            ImageItem item = ItemFactory.CreateImageItem(filePath);
            SetupItem(item);
        }
        /// <summary>
        /// Set ups all the necessities required for an item, including adding to the current scene, creating the respective overlay item and refreshing the item hierarchy.
        /// </summary>
        /// <param name="item">The item to setup</param>
        private void SetupItem(Item item)
        {
            //Add item to current scene
            projectController.AddItemToCurrentScene(item);

            //Notificate ItemHierarchyUI to reload the item hierarchy window to include the new item.
            itemHierarchy.OnSceneChange();

            //Notificate overlay controller to create a new empty overlay item
            overlayController.CreateOverlayItem(item);
        }
    }
}