﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Braver.Domain;
using OdinSerializer;
using System.IO;

namespace Braver.Controller
{
    public class LoadProjectController : MonoBehaviour
    {
       /// <summary>
       /// Loads the last opened project from disk, or creates a new project if none exists.
       /// </summary>
       /// <returns>Project</returns>
        public static Project LoadProject(string projectToOpen)
        {
            var readBytes = File.ReadAllBytes(Application.persistentDataPath + "/" + projectToOpen + ".pr");
            var loadedProject = SerializationUtility.DeserializeValue<Project>(readBytes,DataFormat.JSON);
            return loadedProject;
        }

       public static List<string> LoadProjectNames()
       {
           var readBytes = File.ReadAllBytes(Application.persistentDataPath + "/ProjectList.pr");
            var projectList = SerializationUtility.DeserializeValue<List<string>>(readBytes, DataFormat.JSON);
           return projectList;
        }
    }
}