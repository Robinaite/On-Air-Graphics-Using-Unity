﻿using System.Collections;
using System.Collections.Generic;
using Braver.Domain;
using Braver.Overlay;
using Braver.UI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Braver.Controller
{
    public class InspectorController : MonoBehaviour
    {

        [SerializeField]
        private Transform content;

        [SerializeField]
        private GameObject generalPrefab;

        [SerializeField]
        private GameObject animationsPanelPrefab;

        [SerializeField]
        private List<GameObject> imagePrefabs;

        private Item selectedItem;

        public void LoadItem(Item itemSelected,GameObject itemButton)
        {
            for (int i = content.childCount - 1; i >= 0; i--)
            {
                Destroy(content.GetChild(i).gameObject);
            }

            OverlayController.Instance.SelectItem(itemSelected);

            selectedItem = itemSelected;
            LoadGeneralOptions(itemSelected,itemButton);
            LoadAnimationOptions(itemSelected);
        }


        private void LoadGeneralOptions(Item itemSelected,GameObject itemButton)
        {
            GameObject generalOptions = Instantiate(generalPrefab, content);
            TMP_InputField nameIF = generalOptions.transform.GetChild(1).GetComponentInChildren<TMP_InputField>();
            nameIF.text = itemSelected.name;

            nameIF.onValueChanged.AddListener(x => ChangeName(itemSelected, nameIF,itemButton));

            Transform transformChild = generalOptions.transform.GetChild(2);

            Transform position = transformChild.GetChild(1);
            TMP_InputField[] vec3IF = position.GetComponentsInChildren<TMP_InputField>();
            vec3IF[0].text = itemSelected.position.x.ToString(); //X
            vec3IF[0].onValueChanged.AddListener(x => ChangePositionX(itemSelected,x));

            vec3IF[1].text = itemSelected.position.y.ToString(); //Y
            vec3IF[1].onValueChanged.AddListener(x => ChangePositionY(itemSelected, x));

            vec3IF[2].text = itemSelected.position.z.ToString(); //Z
            vec3IF[2].onValueChanged.AddListener(x => ChangePositionZ(itemSelected, x));


            Transform rotation = transformChild.GetChild(2);
            vec3IF = rotation.GetComponentsInChildren<TMP_InputField>();
            vec3IF[0].text = itemSelected.rotation.x.ToString(); //X
            vec3IF[0].onValueChanged.AddListener(x => ChangeRotationX(itemSelected, x));
            vec3IF[1].text = itemSelected.rotation.y.ToString(); //Y
            vec3IF[1].onValueChanged.AddListener(x => ChangeRotationY(itemSelected, x));
            vec3IF[2].text = itemSelected.rotation.z.ToString(); //Z
            vec3IF[2].onValueChanged.AddListener(x => ChangeRotationZ(itemSelected, x));

            Transform scale = transformChild.GetChild(3);
            vec3IF = scale.GetComponentsInChildren<TMP_InputField>();
            vec3IF[0].text = itemSelected.scale.x.ToString(); //X
            vec3IF[0].onValueChanged.AddListener(x => ChangeScaleX(itemSelected, x));
            vec3IF[1].text = itemSelected.scale.y.ToString(); //Y
            vec3IF[1].onValueChanged.AddListener(x => ChangeScaleY(itemSelected, x));
            vec3IF[2].text = itemSelected.scale.z.ToString(); //Z
            vec3IF[2].onValueChanged.AddListener(x => ChangeScaleZ(itemSelected, x));
        }

        private void ChangeName(Item itemSelected, TMP_InputField nameInputField,GameObject itemButton)
        {
            itemSelected.name = nameInputField.text;
            //Change Text of item Button.
            itemButton.GetComponentInChildren<TextMeshProUGUI>().text = itemSelected.name;
        }

        private void ChangePositionX(Item  itemSelected, string changedValue)
        {
            if (changedValue.Length > 0 || changedValue != "-")
            {
                itemSelected.position.x = float.Parse(changedValue);
                UpdateOverlayItem();
            }
        }
        private void ChangePositionY(Item itemSelected, string changedValue)
        {
            if (changedValue.Length > 0 || changedValue != "-")
            {
                itemSelected.position.y = float.Parse(changedValue);
                UpdateOverlayItem();
            }
        }
        private void ChangePositionZ(Item itemSelected, string changedValue)
        {
            if (changedValue.Length > 0 || changedValue != "-")
            {
                itemSelected.position.z = float.Parse(changedValue);
                UpdateOverlayItem();
            }
        }

        private void ChangeRotationX(Item itemSelected, string changedValue)
        {
            if (changedValue.Length > 0 || changedValue != "-")
            {
                itemSelected.rotation.x = float.Parse(changedValue);
                UpdateOverlayItem();
            }
        }
        private void ChangeRotationY(Item itemSelected, string changedValue)
        {
            if (changedValue.Length > 0 || changedValue != "-")
            {
                itemSelected.rotation.y = float.Parse(changedValue);
                UpdateOverlayItem();
            }
        }
        private void ChangeRotationZ(Item itemSelected, string changedValue)
        {
            if (changedValue.Length > 0 || changedValue != "-")
            {
                itemSelected.rotation.z = float.Parse(changedValue);
                UpdateOverlayItem();
            }
        }

        private void ChangeScaleX(Item itemSelected, string changedValue)
        {
            if (changedValue.Length > 0 || changedValue != "-")
            {
                itemSelected.scale.x = float.Parse(changedValue);
                UpdateOverlayItem();
            }
        }
        private void ChangeScaleY(Item itemSelected, string changedValue)
        {
            if (changedValue.Length > 0 || changedValue != "-")
            {
                itemSelected.scale.y = float.Parse(changedValue);
                UpdateOverlayItem();
            }
        }
        private void ChangeScaleZ(Item itemSelected, string changedValue)
        {
            if (changedValue.Length > 0 || changedValue != "-")
            {
                itemSelected.scale.z = float.Parse(changedValue);
                UpdateOverlayItem();
            }
        }

        private void UpdateOverlayItem()
        {
            OverlayController.Instance.UpdateSelectedItem();
        }

        private void LoadAnimationOptions(Item itemSelected)
        {
            GameObject animPanel = Instantiate(animationsPanelPrefab, content);

            if (itemSelected.HasAnimations())
            {
                animPanel.GetComponent<AnimationPanelUI>().LoadAnimationsPanel(itemSelected.ObtainAnimations());
            }
        }

        public ItemAnimation AddAnimationToCurrentSelectedItem(AnimationTemplate animTempl)
        {
            return selectedItem.CreateNewItemAnimation(animTempl);
        }


    }
}

