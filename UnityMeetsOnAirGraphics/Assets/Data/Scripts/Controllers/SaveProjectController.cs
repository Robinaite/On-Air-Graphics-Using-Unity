﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Braver.Domain;
using OdinSerializer;
using System.IO;

namespace Braver.Controller
{
    public class SaveProjectController : MonoBehaviour
    {
        /// <summary>
        /// Saves the project to the application persistent data path.
        /// </summary>
        /// <param name="project">The project to be saved to file.</param>
        public static void SaveProject(Project project)
        {
            byte[] jsonBytes = SerializationUtility.SerializeValue<Project>(project, DataFormat.JSON);
            File.WriteAllBytes(Application.persistentDataPath + "/" + project.projectName + ".pr", jsonBytes);
            ProgramController.SetLastOpenedProject(project.projectName);
        }

        public static void SaveProjectsList(List<string> projectNames)
        {
            byte[] jsonBytes = SerializationUtility.SerializeValue<List<string>>(projectNames, DataFormat.JSON);
            File.WriteAllBytes(Application.persistentDataPath + "/ProjectList.pr", jsonBytes);
        }
    }
}