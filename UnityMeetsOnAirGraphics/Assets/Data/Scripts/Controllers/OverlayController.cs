﻿using System.Collections;
using System.Collections.Generic;
using Braver.Controller;
using UnityEngine;
using Braver.Domain;
using Braver.UI;
using UnityEngine.Experimental.UIElements;

namespace Braver.Overlay {
    [RequireComponent(typeof(OverlayFactory))]
    public class OverlayController : SingletonBehaviour<OverlayController>
    {
        [SerializeField] //TODO Needs to be removed as should be set dynamically rather then through UI in a later phase when we will start working with multiple scenes.
        private SceneOverlay currentOverlayScene = null;

        [SerializeField]
        private GameObject scenePrefab = null;

        private List<SceneOverlay> sceneOverlayGOList = new List<SceneOverlay>();

        private OverlayFactory factoryGO = null;

        private ItemOverlay selectedItem;

        [SerializeField]
        private AnimationListUI animListUI = null;

        private void Awake()
        {
            factoryGO = GetComponent<OverlayFactory>();
            if(factoryGO == null)
            {
                Debug.LogError("OverlayFactory Script not attached to this OverlayController game object.", this);
            }

            if (scenePrefab == null)
            {
                Debug.LogError("Scene Prefab not attached to this OverlayController game object.", this);
            }
            if (animListUI == null)
            {
                Debug.LogError("animListUI is not attached to this OverlayController game object.", this);
            }
        }

        /// <summary>
        /// Loads a new project for the overlay part
        /// </summary>
        /// <param name="project">The project that got changed too.</param>
        public void LoadProject(Project project)
        {
            for (int i = 1; i < this.transform.childCount; i++)
            {
                Destroy(this.transform.GetChild(i).gameObject);
            }
            sceneOverlayGOList.Clear();

            Scene currentActiveScene = project.ObtainCurrentActiveScene();
            foreach (Scene scene in project.ObtainSceneList())
            {
                bool setCurrentScene = currentActiveScene.Equals(scene);
                CreateNewScene(scene, setCurrentScene);
            }
        }
        /// <summary>
        /// Create an overlay item in the current scene.
        /// </summary>
        /// <param name="item">The item to base the overlay item on.</param>
        public void CreateOverlayItem(Item item)
        {
            GameObject overlayItemGO = currentOverlayScene.CreateOverlayItem(item);
            overlayItemGO.GetComponent<ItemOverlay>().UpdateItem();
        }
        /// <summary>
        /// Creates a new scene overlay item and initializes it. 
        /// </summary>
        /// <param name="createdScene">The respective scene model item</param>
        /// <param name="setCurrentScene">If true, sets the newly created scene as the active scene.</param>
        public void CreateNewScene(Scene createdScene,bool setCurrentScene)
        {
            GameObject overlaySceneGO = Instantiate<GameObject>(scenePrefab, this.transform);
            SceneOverlay sceneOverlay = overlaySceneGO.GetComponent<SceneOverlay>();
            sceneOverlay.InitSceneOverlayGo(createdScene);
            sceneOverlayGOList.Add(sceneOverlay);
            if (setCurrentScene)
            {
                if (currentOverlayScene != null)
                {
                    currentOverlayScene.gameObject.SetActive(false);
                }
                currentOverlayScene = sceneOverlay;
                currentOverlayScene.gameObject.SetActive(true);
            }
        }

        public void ChangeToScene(Scene scene)
        {
            foreach (var sceneOverlay in sceneOverlayGOList)
            {
                if (sceneOverlay.scene.Equals(scene))
                {
                    currentOverlayScene.gameObject.SetActive(false);
                    currentOverlayScene = sceneOverlay;
                    currentOverlayScene.gameObject.SetActive(true);
                    animListUI.ResetContent();
                    AnimationController.OnSceneOrModeChange();
                    ExecuteOnSceneChangeAnimation(currentOverlayScene.transform);
                    break;
                }
            }
        }

        private void ExecuteOnSceneChangeAnimation(Transform currentOverlayItem)
        {
            if (ProgramController.IsInBroadcastMode())
            {
                
                for (int i = 0; i < currentOverlayItem.childCount; i++)
                {
                    ItemOverlay overlayItem = currentOverlayItem.GetChild(i).gameObject.GetComponent<ItemOverlay>();
                    Item item = overlayItem.ObtainItem();
                    if (item.HasAnimations())
                    {
                        bool alsoExecuteFollowingAnim = false;
                        GameObject currentAnimButton = null;
                        AnimExecution previousExecutedAnim = AnimExecution.Manual;
                        foreach (var itemAnim in item.ObtainAnimations())
                        {
                            //OnSceneLoadAnimation
                            if (itemAnim.animExecution == AnimExecution.OnSceneLoad)
                            {
                                previousExecutedAnim = AnimExecution.OnSceneLoad;
                                AnimationController.AnimatePosition(itemAnim, overlayItem.transform);
                                alsoExecuteFollowingAnim = true;
                            }
                            else if(itemAnim.animExecution == AnimExecution.Manual)
                            {
                                previousExecutedAnim = AnimExecution.Manual;
                                    currentAnimButton = animListUI.CreateAnimButton(itemAnim, overlayItem.transform);
                            }
                            //else if(itemAnim.animExecution == AnimExecution.TogetherWithAbove)
                            //{
                            //    if (previousExecutedAnim == AnimExecution.OnSceneLoad)
                            //    {
                            //        AnimationController.AnimatePosition(itemAnim, overlayItem.transform);
                            //    } else if (previousExecutedAnim == AnimExecution.Manual)
                            //    {
                            //        if (currentAnimButton != null)
                            //            currentAnimButton.GetComponent<ButtonAnimation>()
                            //                .AddAnimationsOnCurrentOverlayItem(itemAnim);
                            //    }
                            //}
                            else
                            {
                                currentAnimButton = null;
                                alsoExecuteFollowingAnim = false;
                            }
                        }
                    }
                    if (item.HasChildrenItems())
                    {
                        ExecuteOnSceneChangeAnimation(overlayItem.transform);
                    }
                }
            }
        }

        public void DeleteSceneOverlay(Scene scene)
        {
            foreach (var sceneOverlay in sceneOverlayGOList)
            {
                if (sceneOverlay.scene.Equals(scene))
                {
                    Destroy(sceneOverlay.gameObject);
                    break;
                }
            }
        }
        public void UpdateSortingOrder()
        {
            currentOverlayScene.UpdateSortingOrder();
        }

        public void SelectItem(Item itemSelected)
        {
            for (int i = currentOverlayScene.transform.childCount - 1; i >= 0; i--)
            {
                ItemOverlay itemOverlay = currentOverlayScene.transform.GetChild(i).gameObject.GetComponent<ItemOverlay>();
                if (itemOverlay.Equals(itemSelected))
                {
                    selectedItem = itemOverlay;
                    return;
                }
                if (FindOverlayItem(itemSelected, currentOverlayScene.transform.GetChild(i)))
                {
                    return;
                }
            }
        }

        private bool FindOverlayItem(Item itemSelected,Transform transform)
        {
            for (int i = transform.childCount - 1; i >= 0; i--)
            {
                ItemOverlay itemOverlay = transform.GetChild(i).gameObject.GetComponent<ItemOverlay>();
                if (itemOverlay.Equals(itemSelected))
                {
                    selectedItem = itemOverlay;
                    return true;
                }

                return FindOverlayItem(itemSelected, transform.GetChild(i));
            }
            return false;
        }

        public void UpdateSelectedItem()
        {
            selectedItem.UpdateItem();
        }
    }
}