﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Braver.Controller
{
    /// <summary>
    /// Controlls everything related to the broadcast mode.
    /// </summary>
    public class BroadcastModeController : MonoBehaviour
    {
        [SerializeField]
        public GameObject outputObject = null;
        private GameObject outputObjectClone = null;
        [SerializeField]
        private GameObject startStopOutputButton = null;
        private void OnEnable()
        {
            if (outputObject != null)
            {
                outputObjectClone = Instantiate(outputObject);
            }
        }

        private void OnDisable()
        {
            if (outputObject != null)
            {
                Destroy(outputObjectClone);
            }
        }
        /// <summary>
        /// Starts or stops the output by setting the object active stat.
        /// </summary>
        public void StartStopOutput()
        {
            if(outputObject != null)
            {
                if(outputObjectClone == null)
                {
                    outputObjectClone = Instantiate(outputObject);
                }
                if (outputObjectClone.activeInHierarchy) //Stop output
                {
                    startStopOutputButton.GetComponentInChildren<TextMeshProUGUI>().SetText("Start Stream");
                    outputObjectClone.SetActive(false);
                }
                else //Start output
                {
                    startStopOutputButton.GetComponentInChildren<TextMeshProUGUI>().SetText("Stop Stream");
                    outputObjectClone.SetActive(true);
                }
            } else
            {
                ///TODO add warning panel.
                Debug.LogError("Output Settings not set.");
            }
        }
    }
}