﻿using System;
using System.Collections;
using System.Collections.Generic;
using Braver.Domain;
using Braver;
using Braver.Overlay;
using UnityEngine;

namespace Braver.Controller
{
    public class ProgramController : SingletonBehaviour<ProgramController>
    {
        #region Program Mode Static
        private static ProgramMode currentMode = ProgramMode.Create;
        /// <summary>
        /// Check if the program is in broadcast mode.
        /// </summary>
        /// <returns>True if is in broadcast mode.</returns>
        public static bool IsInBroadcastMode()
        {
            return currentMode == ProgramMode.Broadcast;
        }
        /// <summary>
        /// Check if the program is in create mode.
        /// </summary>
        /// <returns>True if in create mode.</returns>
        public static bool IsInCreateMode()
        {
            return currentMode == ProgramMode.Create;
        }
        /// <summary>
        /// Change the program mode to broadcast mode. 
        /// </summary>
        public static void ChangeToBroadcastMode()
        {
            if(currentMode != ProgramMode.Broadcast)
            {
                currentMode = ProgramMode.Broadcast;
            }
        }
        /// <summary>
        /// Change the program mode to create mode.
        /// </summary>
        public static void ChangeToCreateMode()
        {
            if (currentMode != ProgramMode.Create)
            {
                AnimationController.OnSceneOrModeChange();
                currentMode = ProgramMode.Create;
            }
        }
        #endregion

        #region Saved Projects and Last Opened static
        private static string lastOpenedProjectName = null;
        private static List<string> savedProjects = new List<string>();

        /// <summary>
        /// Sets the last opened project and adds it to the savedProjects list if it is a new project.
        /// </summary>
        /// <param name="projectName">The project name that is constated in the project item on save.</param>
        public static void SetLastOpenedProject(string projectName)
        {
            if (!savedProjects.Contains(projectName))
            {
                savedProjects.Add(projectName);
            }

            PlayerPrefs.SetString("lastOpenedProjectName", projectName);
            if(lastOpenedProjectName != projectName) 
            {
                PlayerPrefs.Save(); //Saves the choice to disk on project change. 
            }
            lastOpenedProjectName = projectName;
        }

        public static void SaveProjectListNames()
        {
            SaveProjectController.SaveProjectsList(savedProjects);
        }

        public static void LoadProjectListNames()
        {
            savedProjects = LoadProjectController.LoadProjectNames();
        }

        public static List<string> ObtainSavedProjectList()
        {
            return savedProjects;
        }
        /// <summary>
        /// Obtains the last opened project name. In case the first time opening tries to obtain from PlayerPrefs the last opened project.
        /// </summary>
        /// <returns>The last opened project name. Or default value if none.</returns>
        public static string ObtainLastOpenedProject()
        {
            if(lastOpenedProjectName == null)
            {
                lastOpenedProjectName = PlayerPrefs.GetString("lastOpenedProjectName");
            }
            return lastOpenedProjectName;
        }
        #endregion

        [SerializeField]
        public BroadcastModeController broadcastModeController;

        [SerializeField]
        private List<GameObject> outputTypesPrefabs = new List<GameObject>();

        [SerializeField]
        private List<AnimationTemplate> animationTemplates = new List<AnimationTemplate>();

        /// <summary>
        /// Sets the output type in the broadcast controller to the output selected in the settings.
        /// </summary>
        /// <param name="outputType">The output type</param>
        public void SetOutputType(OutputType outputType)
        {
            switch (outputType)
            {
                case OutputType.NDI:
                    broadcastModeController.outputObject = outputTypesPrefabs[0];
                    break;
                case OutputType.Spout:
                    broadcastModeController.outputObject = outputTypesPrefabs[1];
                    break;
                default:
                    broadcastModeController.outputObject = null;
                    break;
            }
        }

        public List<AnimationTemplate> ObtainAnimationTemplates()
        {
            return animationTemplates;
        }

    }
    public enum ProgramMode { Create, Broadcast }
}