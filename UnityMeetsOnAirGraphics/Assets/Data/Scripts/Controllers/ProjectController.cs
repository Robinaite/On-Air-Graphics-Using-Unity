﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Braver.Domain;
using Braver.Overlay;

namespace Braver.Controller {
    public class ProjectController : SingletonBehaviour<ProjectController>
    {
        private ProgramController programController = null;

        [SerializeField] //TODO Only for debugging purpose need to remove the serializeField later.
        public Project currentProject = new Project();

        [SerializeField]
        private UI.ItemHierarchy itemHierarchy = null;

        [SerializeField]
        private UI.SceneList sceneList = null;

        private Overlay.OverlayController overlayController = null;

        private void Awake()
        {
            programController = ProgramController.Instance;
            overlayController = OverlayController.Instance;
            if (itemHierarchy == null)
            {
                Debug.LogError("There's no itemHierarchy script associated to the project controller!", this);
            }
            if (sceneList == null)
            {
                Debug.LogError("There's no Scene List script associated to the project controller!", this);
            }
            if (overlayController == null)
            {
                Debug.LogError("There's no OverlayController script associated to the project controller!", this);
            }
        }

        private void Start()
        {
            LoadLastOpenedProject();
            ProgramController.LoadProjectListNames();
        }

        private void OnApplicationQuit()
        {
            //Save the current project
            SaveCurrentProject();
            ProgramController.SaveProjectListNames();
        }

        /// <summary>
        /// Loads the last opened project or creates a new project if none is found.
        /// </summary>
        private void LoadLastOpenedProject()
        {
            try
            {
                Project lastLoadedProject =
                    LoadProjectController.LoadProject(ProgramController.ObtainLastOpenedProject());
                ChangeProject(lastLoadedProject);
            }
            catch (FileNotFoundException ex)
            {
                Debug.Log("Last Project file not found. + " + ex.Message);
                Project lastLoadedProject = new Project();
                ChangeProject(lastLoadedProject);
            }
        }
        /// <summary>
        /// Alerts all other scripts on project change. (Can be improved using the observer pattern if time is found)
        /// </summary>
        /// <param name="project">The project to change too.</param>
        public void ChangeProject(Project project)
        {
            currentProject = project;
            sceneList.LoadProject(project);
            overlayController.LoadProject(currentProject);
        }

        /// <summary>
        /// Asks the current project to add an item to the current scene.
        /// </summary>
        /// <param name="item">The item to be added.</param>
        public void AddItemToCurrentScene(Item item)
        {
            currentProject.AddItemToCurrentScene(item);
        }
        /// <summary>
        /// Obtains the item hierarchy UI Script object.
        /// </summary>
        /// <returns>Item Hierarchy UI Script Object</returns>
        public UI.ItemHierarchy ObtainItemHierarchy()
        {
            return itemHierarchy;
        }
        /// <summary>
        /// Obtains the Overlay Controller Script Object
        /// </summary>
        /// <returns>Overlay Controller Script Object</returns>
        public Overlay.OverlayController ObtainOverlayController()
        {
            return overlayController;
        }

        /// <summary>
        /// Sets the output type on the project item. And asks the program to setup the respective output type.
        /// </summary>
        /// <param name="outputType">The output type to set</param>
        private void SetOutputType(OutputType outputType)
        {
            currentProject.outputType = outputType;
            programController.SetOutputType(outputType);
        }
        /// <summary>
        /// Method that will be called by the UI, passes the index of the dropdown selected. 
        /// Should be:
        /// 1 - NDI
        /// 2 - Spout
        /// Other Num - Not set.
        /// </summary>
        /// <param name="uiIndexSelectedType"></param>
        public void SetOutputType(int uiIndexSelectedType)
        {
            switch (uiIndexSelectedType)
            {
                case 1:
                    SetOutputType(OutputType.NDI);
                    break;
                case 2:
                    SetOutputType(OutputType.Spout);
                    break;
                default:
                    SetOutputType(OutputType.None);
                    break;
            }
        }
        /// <summary>
        /// Creates a new scene in the project.
        /// </summary>
        public void CreateNewScene()
        {
            Scene createdScene = currentProject.CreateNewScene(true);
            sceneList.CreateNewScene(createdScene);
            overlayController.CreateNewScene(createdScene, true);
        }

        public void DeleteCurrentScene()
        {
            DeleteScene(currentProject.ObtainCurrentActiveScene());
        }

        public void DeleteScene(Scene selectedScene)
        {
            if (currentProject.RemoveScene(selectedScene))
            {
                if (currentProject.ObtainSceneList().Count < 1)
                {
                    CreateNewScene();
                }
                else
                {
                    if (currentProject.ObtainCurrentActiveScene().Equals(selectedScene))
                    {
                        currentProject.SetActiveScene(currentProject.ObtainSceneList()[0]);
                        overlayController.ChangeToScene(currentProject.ObtainCurrentActiveScene());
                    }
                }
                overlayController.DeleteSceneOverlay(selectedScene);
                sceneList.LoadProject(currentProject);
            }
        }

        public void ChangeScene(Scene selectedScene)
        {
            currentProject.SetActiveScene(selectedScene);
        }

        public void SaveCurrentProject()
        {
            SaveProjectController.SaveProject(currentProject);
        }

        public bool RemoveItemFromScene(Item item)
        {
            return currentProject.RemoveItemFromCurrentScene(item);
        }
    }

}