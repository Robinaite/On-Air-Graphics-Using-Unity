﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ErrorPanel : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI errorText = null;
    [SerializeField]
    private GameObject errorPanel = null;

    public void ShowError(string errorMsg)
    {
        errorText.SetText(errorMsg);
        errorPanel.SetActive(true);
    }

}
