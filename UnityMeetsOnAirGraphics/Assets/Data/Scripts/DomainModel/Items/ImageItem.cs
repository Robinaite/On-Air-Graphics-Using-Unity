﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Braver.Domain
{
    public class ImageItem : Item
    {
        [Header("Specific Options")]
        [SerializeField]
        public string imagePath = "";
    }
}