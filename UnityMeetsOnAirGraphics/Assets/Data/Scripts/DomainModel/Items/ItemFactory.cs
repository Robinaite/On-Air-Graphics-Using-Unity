﻿using UnityEngine;

namespace Braver.Domain
{
    public class ItemFactory
    {
        /// <summary>
        /// Creates the empty item and sets a temporary name.
        /// </summary>
        /// <returns>Empty item SO</returns>
        public static Item CreateEmptyItem()
        {
            Item itemCreated = new Item();
            itemCreated.name = "DefaultItem-";
            return itemCreated;
        }
        /// <summary>
        /// Creates an image item and sets a temporary name.
        /// </summary>
        /// <param name="imagePath">The path till the file of the image</param>
        /// <returns>The Domain Image Item</returns>
        public static ImageItem CreateImageItem(string imagePath)
        {
            ImageItem itemCreated = new ImageItem();
            itemCreated.imagePath = imagePath;
            itemCreated.name = "DefaultName-" + itemCreated.globalOrder;
            return itemCreated;
        }
    }
}