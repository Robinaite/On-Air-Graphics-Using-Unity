﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OdinSerializer;
using System;

namespace Braver.Domain
{
    [System.Serializable]
    public class Project
    {
        [SerializeField]
        public string projectName;
        [SerializeField]
        public OutputType outputType;
        [SerializeField]
        private List<Scene> sceneList = new List<Scene>();
        [SerializeField]
        private Scene currentActiveScene = null;

        public Project()
        {
            Scene firstScene = new Scene();
            sceneList.Add(firstScene);
            currentActiveScene = firstScene;
            this.projectName = "FirstProject";
        }

        /// <summary>
        /// Obtains the current active scene in this project.
        /// </summary>
        /// <returns>The current active scene</returns>
        public Scene ObtainCurrentActiveScene()
        {
            return currentActiveScene;
        }
        /// <summary>
        /// Executes the adding of a scene item on the currentActiveScene
        /// </summary>
        /// <param name="itemToAdd">The item to be added to the current active scene.</param>
        public void AddItemToCurrentScene(Item itemToAdd)
        {
            currentActiveScene.AddSceneItem(itemToAdd);
        }

        public void SetActiveScene(Scene scene)
        {
            if (sceneList.Contains(scene))
            {
                currentActiveScene = scene;
            }
        }
        /// <summary>
        /// Obtains the scene list of the project
        /// </summary>
        /// <returns>The scene list</returns>
        public List<Scene> ObtainSceneList()
        {
            return sceneList;
        }
        
        /// <summary>
        /// Creates a new scene
        /// </summary>
        /// <param name="setActiveScene">Sets the newly created scene as the active scene in the project.</param>
        /// <returns>The newly created scene.</returns>
        public Scene CreateNewScene(bool setActiveScene)
        {
            Scene newlyCreatedScene = new Scene();
            sceneList.Add(newlyCreatedScene);
            if (setActiveScene)
            {
                currentActiveScene = newlyCreatedScene;
            }
            return newlyCreatedScene;
        }

        public bool RemoveScene(Scene selectedScene)
        {
            return sceneList.Remove(selectedScene);
        }

        public bool RemoveItemFromCurrentScene(Item item)
        {
            return currentActiveScene.RemoveItem(item);
        }
    }
    public enum OutputType { None,NDI, Spout }
}