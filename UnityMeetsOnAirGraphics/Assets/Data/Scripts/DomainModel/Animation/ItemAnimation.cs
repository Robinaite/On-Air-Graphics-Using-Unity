﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Braver.Domain
{
    public class ItemAnimation
    {
        [SerializeField]
        private AnimationParameters animParameter;

        [SerializeField]
        public AnimExecution animExecution;

        [SerializeField]
        private AnimationRuntime animation = null;

        [SerializeField]
        private static int ANIMATION_ID = 0;

        [SerializeField]
        [HideInInspector]
        private readonly int animID;

        public ItemAnimation(AnimationTemplate animTemp)
        {
            InitFromTemplate(animTemp);
            animID = ANIMATION_ID;
            animExecution = AnimExecution.Manual;
            ANIMATION_ID++;
        }

        public int AnimId => animID;
        
        private void InitFromTemplate(AnimationTemplate animTemp)
        {
            animation = new AnimationRuntime();
            animParameter = animTemp.ObtainAnimationParameter();
            animation.value = animTemp.ObtainDefaultValue();
            animation.duration = animTemp.ObtainDefaultDuration();
            animation.fromCurrentValue = animTemp.ObtainFromCurrentValue();
        }

        public void ChangeValue(Vector3 newStartValue)
        {
            animation.value = newStartValue;
        }

        public void ChangeFromCurrentValue(bool newFromCurrentValue)
        {
            animation.fromCurrentValue = newFromCurrentValue;
        }
        public void ChangeDuration(float newDuration)
        {
            animation.duration = newDuration;
        }

        public Vector3 ObtainValue()
        {
            return animation.value;
        }

        public float ObtainDuration()
        {
            return animation.duration;
        }

        public bool IsTypeAnimationParameter(AnimationParameters param)
        {
            return param.Equals(animParameter);
        }

        public bool IsFromCurrentPosition()
        {
            return animation.fromCurrentValue;
        }

        protected bool Equals(ItemAnimation other)
        {
            return animID == other.animID;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ItemAnimation) obj);
        }

        public override int GetHashCode()
        {
            return animID;
        }
    }

    public enum AnimationParameters { Position,Rotation,Scale }
    public enum AnimExecution { Manual, OnSceneLoad}
}

