﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Braver.Controller;
using Braver.Domain;
using TMPro;

namespace Braver.UI
{
    public class buttonScript : MonoBehaviour
    {
        private ProjectController projectController;
        private Braver.Domain.Project project;

        [SerializeField]
        private TMP_InputField inputField = null;

        private void Awake()
        {
            projectController = ProjectController.Instance;
            if (inputField == null)
            {
                Debug.LogError("Input field is missing attached to this button",this);
            }
        }

        public void SaveProject()
        {
            Braver.Controller.SaveProjectController.SaveProject(projectController
                .currentProject);
        }

        public void CreateNewProject()
        {
            //First save current project.
            projectController.SaveCurrentProject();

            //CreateNewProject
            Project project = new Project();
            project.projectName = inputField.text;

            projectController.ChangeProject(project);
        }


        public void LoadProject()
        {
            //projectController.GetComponent<ProjectController>().currentProject = Braver.Controller.LoadProjectController.LoadProject();
        }
    }
}