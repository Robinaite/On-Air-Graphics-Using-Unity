﻿using System.Collections;
using System.Collections.Generic;
using Braver.Domain;
using UnityEngine;

namespace Braver.Overlay
{
    public class SceneOverlay : MonoBehaviour
    {
        [SerializeField] //TEMP PUBLIC FOR DEBUGGING PURP
        public Braver.Domain.Scene scene;

        private OverlayFactory factoryGO = null;

        private bool loadedScene = false;

        private void Awake()
        {
            factoryGO = GetComponentInParent<OverlayFactory>();
            if (factoryGO == null)
            {
                Debug.LogError("OverlayFactory Script not attached to this OverlayController game object.", this);
            }
        }
        
        private void OnEnable()
        {
            if (!loadedScene) //Only loads the scene if it didn't load before.
            {
                LoadScene();
            }
        }
        /// <summary>
        /// Initializes the scene overlay. 
        /// </summary>
        /// <param name="sceneToSet">The scene to set the respective overlay scene too.</param>
        public void InitSceneOverlayGo(Braver.Domain.Scene sceneToSet)
        {
            scene = sceneToSet;
        }

        public GameObject CreateOverlayItem(Item item)
        {
            return CreateOverlayItem(item, this.transform);
        }
        /// <summary>
        /// Creates the overlay item through the factory and sets its parent to the respective scene.
        /// </summary>
        /// <param name="item">The item to create a gameObject for.</param>
        private GameObject CreateOverlayItem(Item item,Transform parentTransform)
        {
            if (factoryGO == null)
            {
                factoryGO = GetComponentInParent<OverlayFactory>();
            }
            GameObject itemGOCreated = null;
            if (item.GetType() == typeof(Item))
            {
                itemGOCreated = factoryGO.CreateEmptyOverlayItem(item);
            }
            else if (item.GetType() == typeof(ImageItem))
            {
                itemGOCreated = factoryGO.CreateImageOverlayItem(item as ImageItem);
            }
            if (itemGOCreated != null)
            {
                itemGOCreated.transform.SetParent(parentTransform,true);
            }
            
            return itemGOCreated;
        }
        /// <summary>
        /// Creates the respective overlay scene from the scene item settings.
        /// </summary>
        private void LoadScene()
        {

            for (int i = this.transform.childCount-1; i > 0; i--)
            {
                Destroy(this.transform.GetChild(i).gameObject);
            }

            foreach (Item item in scene.ObtainSceneItems())
            {
                if (item.HasChildrenItems())
                {
                    LoadChildrenItemsRecursive(item,this.transform);
                }
                else
                {
                    CreateOverlayItem(item,this.transform);
                }
            }
            loadedScene = true;
        }
        /// <summary>
        /// Recursive method in case the item has children items.
        /// </summary>
        /// <param name="fatherItem"></param>
        private void LoadChildrenItemsRecursive(Item fatherItem,Transform parentTransform)
        {
            GameObject fatherObject = CreateOverlayItem(fatherItem,parentTransform);
            foreach (Item childItem in fatherItem.ObtainChildrenItems())
            {
                LoadChildrenItemsRecursive(childItem,fatherObject.transform);
            }
        }

        public void UpdateSortingOrder()
        {
            LoadScene();
        }
    }
}