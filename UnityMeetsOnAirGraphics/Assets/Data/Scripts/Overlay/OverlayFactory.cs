﻿using Braver.Domain;
using UnityEngine;
namespace Braver.Overlay
{
    public class OverlayFactory : MonoBehaviour
    {
        [SerializeField]
        private GameObject emptyItem = null;
        [SerializeField]
        private GameObject imageItem = null;

        private void Awake()
        {
            if(emptyItem == null)
            {
                Debug.LogError("No prefab for empty overlay item added to this factory.", this);
            }
            if (imageItem == null)
            {
                Debug.LogError("No prefab for image overlay item added to this factory.", this);
            }
        }
        /// <summary>
        /// Creates the empty overlay Item and sets the respective SO to the overlay item created.
        /// </summary>
        /// <param name="item">The item scriptable object</param>
        /// <returns>A clone of the emptyItem prefab, with setup</returns>
        public GameObject CreateEmptyOverlayItem(Item item)
        {
            GameObject itemCreated = Instantiate(emptyItem);
            itemCreated.GetComponent<ItemOverlay>().SetupItemOverlayGO(item);

            return itemCreated;
        }
        /// <summary>
        /// Creates the image overlay item and asks to set it up as needed. This will execute the respective setup function of the class.
        /// </summary>
        /// <param name="item">The item to create an overlay item for</param>
        /// <returns>The image overlay item created</returns>
        public GameObject CreateImageOverlayItem(ImageItem item)
        {
            GameObject itemCreated = Instantiate(imageItem);
            itemCreated.GetComponent<ImageItemOverlay>().SetupItemOverlayGO(item);
            return itemCreated;
        }
    }
}