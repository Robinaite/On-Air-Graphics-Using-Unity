﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Braver.Domain;
using UnityEngine;
using DG.Tweening;

namespace Braver.Overlay
{
    public class AnimationController
    {
        public static void AnimatePosition(ItemAnimation anim, Transform transformToAnimate)
        {
            if (anim.IsFromCurrentPosition())
            {
                DOTween.To(() => transformToAnimate.localPosition, x => transformToAnimate.localPosition = x, anim.ObtainValue(),anim.ObtainDuration()).SetId(anim.AnimId);
                //transformToAnimate.DOMove(anim.ObtainValue(), anim.ObtainDuration()).SetId(anim.AnimId);
            }
            else
            {
                Vector3 currentPosition = transformToAnimate.localPosition;
                DOTween.To(() => transformToAnimate.localPosition, x => transformToAnimate.localPosition = x, anim.ObtainValue(), anim.ObtainDuration()).From()
                    .OnRewind(() => OnResetTransformFrom(transformToAnimate, currentPosition)).SetId(anim.AnimId);
               // transformToAnimate.DOMove(anim.ObtainValue(), anim.ObtainDuration()).From()
                  //  .OnRewind(() => OnResetTransformFrom(transformToAnimate,currentPosition)).SetId(anim.AnimId);
            }
        }

        public static void OnSceneOrModeChange()
        {
            DOTween.RewindAll();
        }

        private static void OnResetTransformFrom(Transform transform, Vector3 originalPos)
        {
            transform.localPosition = originalPos;
        }

    }
}

