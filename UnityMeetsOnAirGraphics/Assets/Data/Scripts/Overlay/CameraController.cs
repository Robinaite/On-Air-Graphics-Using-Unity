﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CameraController : MonoBehaviour
{

    /// for the sizing  of the unity camera and pixels and such, we need to watch out for the following:
    /// - The sprites need to have their pixel to units set to 1. 
    /// - There will be two different things, output size and canvas size.
    /// - Output size is the width x height of the render texture asset item(atm called CurrentSceneRendered).
    /// - The canvas size needs to be updated in two places: 1 - the OverlayCameraPlacement Aspect Ratio fitter 2-OverlayCamera size is equal to half the height of the screen wanted.
    /// Need to check how the camera gets updated in real-time


    // Update is called once per frame
    void Update()
    {
        
    }
}
