﻿using System.Collections;
using System.Collections.Generic;
using Braver.Domain;
using UnityEngine;

namespace Braver.Overlay
{
    public class ItemOverlay : MonoBehaviour
    {
        [SerializeField]
        protected Braver.Domain.Item itemSO;
        /// <summary>
        /// Setups the item Overlay created.
        /// </summary>
        /// <param name="itemToSet">The item SO to set it relative too.</param>
        public virtual void SetupItemOverlayGO(Braver.Domain.Item itemToSet)
        {
            itemSO = itemToSet;
            UpdateItem();
        }

        public bool Equals(Item otherItem)
        {
            return itemSO.Equals(otherItem);
        }

        public virtual void UpdateItem()
        {
            UpdateTransform();
        }

        private void UpdateTransform()
        {
            Transform thisTransform = this.transform;
            thisTransform.localPosition = itemSO.position;
            thisTransform.localEulerAngles = itemSO.rotation;
            thisTransform.localScale = itemSO.scale;
        }

        public Item ObtainItem()
        {
            return itemSO;
        }

    }
}
