﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using System.Threading.Tasks;
using Braver.Domain;
using UnityEngine;

namespace Braver.Overlay
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class ImageItemOverlay : ItemOverlay
    {
        private SpriteRenderer spriteRenderer = null;

        //DefaultSpriteSettings
        private static readonly float DEFAULT_PIXELS_PER_UNIT = 1f;
        private static readonly Vector2 DEFAULT_PIVOT = new Vector2(0.5f, 0.5f); //Center Pivot
        private static readonly uint DEFAULT_EXTRUDE_VALUE = 0;
        private static readonly SpriteMeshType DEFAULT_SPRITE_MESH_TYPE = SpriteMeshType.FullRect;

        private void Awake()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            if(spriteRenderer.drawMode != SpriteDrawMode.Sliced)
            {
                spriteRenderer.drawMode = SpriteDrawMode.Sliced;
                Debug.LogWarning("This object does not have the sprite rendered in default on Sliced, which could affect performance setting it each time, please set it by default!", this);
            }
            if(spriteRenderer == null)
            {
                Debug.LogError("This should never happen, this script needs to have on the same Game Object its attached to a sprite renderer.", this);
            }
        }

        /// <summary>
        /// Overriden method of the base item, beside doing the base methods, import the image semi-asynchronously from disk.
        /// </summary>
        /// <param name="itemToSet">The domain item to get the default settings from.</param>
        public override void SetupItemOverlayGO(Item itemToSet) 
        {
            base.SetupItemOverlayGO(itemToSet);
            int spriteRenderingOrder = itemSO.globalOrder;

            spriteRenderer.sortingOrder = -spriteRenderingOrder;
            StartCoroutine(LoadImageFromDisk(((Braver.Domain.ImageItem)itemSO).imagePath, CreateNewSprite));
        }
        /// <summary>
        /// Semi-Async method to load the image from disk into a unity texture2D. Executes the funciont on callback if texture is obtained.
        /// </summary>
        /// <param name="filePath">The file path to obtain the file from.</param>
        /// <param name="textureObtainedCallback">The method to call on successly obtaining the texture.</param>
        /// <returns></returns>
        IEnumerator LoadImageFromDisk(string filePath, System.Action<Texture2D> textureObtainedCallback)
        {
            using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(filePath))
            {
                yield return uwr.SendWebRequest();

                if (uwr.isNetworkError || uwr.isHttpError)
                {
                    Debug.Log(uwr.error);
                }
                else
                {
                    var texture = DownloadHandlerTexture.GetContent(uwr);
                    textureObtainedCallback(texture);
                }
            }
        }
        /// <summary>
        /// Creates a unity sprite based on the texture obtained.
        /// </summary>
        /// <param name="tex2D">Texture of the sprite.</param>
        private void CreateNewSprite(Texture2D tex2D)
        {
            if (tex2D != null)
            {
                Rect texRect = new Rect(0f, 0f, tex2D.width, tex2D.height);
                Sprite newSprite = Sprite.Create(tex2D, texRect, DEFAULT_PIVOT, DEFAULT_PIXELS_PER_UNIT, DEFAULT_EXTRUDE_VALUE, DEFAULT_SPRITE_MESH_TYPE);
                SetupSpriteRenderer(newSprite);
            } else
            {
                Debug.LogWarning("Error with obtaining the texture.");
            }
        }
        /// <summary>
        /// Sets up the sprite renderer with the new sprite and its respective size.
        /// </summary>
        /// <param name="sprite">The sprite to render.</param>
        private void SetupSpriteRenderer(Sprite sprite)
        {
            if (sprite == null)
            {
                Debug.LogError("Unable to read this image from the path", this);
                return;
            }
            spriteRenderer.sprite = sprite;
            spriteRenderer.size = new Vector2(sprite.texture.width, sprite.texture.height);

            UpdateItem();
        }

        public void UpdateSpriteRendererOrder(int order)
        {
            spriteRenderer.sortingOrder = order;
        }
    }
}
