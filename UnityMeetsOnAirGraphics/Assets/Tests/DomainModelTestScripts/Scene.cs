﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Braver.Domain
{
    /// <summary>
    /// Scene domain model item. 
    /// </summary>
    [System.Serializable]
    public class Scene
    {
        [SerializeField] //TEMP PUBLIC FOR DEBUGGING PURPOSE
        public string name;

        private static int SCENE_COUNT = 0;
        [SerializeField]
        private int sceneID;

        [SerializeField]
        private List<Item> sceneItems = new List<Item>();

        public Scene()
        {
            name = "Scene1";
            sceneID = SCENE_COUNT;
            SCENE_COUNT++;
        }

        /// <summary>
        /// Obtains all the main items of this scene. Each Item has the knowledge if it has child items. 
        /// </summary>
        /// <returns>Main Scene Items List.</returns>
        public List<Item> ObtainSceneItems()
        {
            return sceneItems;
        }
        /// <summary>
        /// Add an to this scene.
        /// </summary>
        /// <param name="itemToAdd">Item to add.</param>
        public void AddSceneItem(Item itemToAdd)
        {
            sceneItems.Add(itemToAdd);
            SortLists();
        }

        public override bool Equals(object obj)
        {

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            Scene otherScene = (Scene)obj;

            if(otherScene.sceneID != this.sceneID)
            {
                return false;
            }
            return true;
        }

        public override int GetHashCode()
        {
            return -1826259186 + sceneID.GetHashCode();
        }

        public void SortLists()
        {
            sceneItems.Sort();
            foreach (var sceneItem in sceneItems)
            {
                if (sceneItem.HasChildrenItems())
                {
                    sceneItem.OrderChildItems();
                }
            }
        }

        public bool RemoveItem(Item itemToRemove)
        {
            if (sceneItems.Remove(itemToRemove))
            {
                return true;
            }
            else
            {
                foreach (var sceneItem in sceneItems)
                {
                    if (sceneItem.HasChildrenItems())
                    {
                        return sceneItem.RemoveItem(itemToRemove);
                    }
                }
                return false;
            }
        }
        /// <summary>
        /// Increases all the orders of the items.
        /// </summary>
        /// <param name="itemHovered">The starting item to increase localOrder.</param>
        /// <param name="startingItem">Include the item passed through parameter in increasing its localOrder.</param>
        public void IncreaseOrderItem(Item itemHovered,bool startingItem)
        {
            bool findItem = false;
            foreach (var childrenItem in sceneItems)
            {
                if (!findItem)
                {
                    findItem = childrenItem.Equals(itemHovered);
                    if (!findItem && childrenItem.HasChildrenItems())
                    {
                        findItem = childrenItem.IncreaseOrderItem(itemHovered, startingItem);
                        if (findItem)
                        {
                            continue;
                        }
                    }

                    if (findItem && startingItem)
                    {
                        childrenItem.globalOrder++;
                    }
                }
                else
                {
                    childrenItem.globalOrder++;
                }
            }
        }
    }
}

