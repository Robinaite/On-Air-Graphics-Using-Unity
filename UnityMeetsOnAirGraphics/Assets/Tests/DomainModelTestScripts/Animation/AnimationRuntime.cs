﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Braver.Domain
{
    [System.Serializable]
    public class AnimationRuntime
    {
        [SerializeField]
        public Vector3 value = Vector3.zero;

        [SerializeField]
        public float duration = 0;

        [SerializeField]
        public bool fromCurrentValue = true;
    }
}

