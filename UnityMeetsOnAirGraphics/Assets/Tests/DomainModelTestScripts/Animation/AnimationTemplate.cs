﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Braver.Domain
{
    [CreateAssetMenu(fileName = "animationTemplate",menuName = "Braver/Create Animation Template")]
    public class AnimationTemplate : ScriptableObject
    {
        [SerializeField]
        public AnimationParameters animationParameter;
        [SerializeField]
        public AnimationRuntime defaultValues = new AnimationRuntime();
        public Vector3 ObtainDefaultValue()
        {
            return defaultValues.value;
        }

        public float ObtainDefaultDuration()
        {
            return defaultValues.duration;
        }

        public AnimationParameters ObtainAnimationParameter()
        {
            return animationParameter;
        }

        public bool ObtainFromCurrentValue()
        {
            return defaultValues.fromCurrentValue;
        }
    }
}

