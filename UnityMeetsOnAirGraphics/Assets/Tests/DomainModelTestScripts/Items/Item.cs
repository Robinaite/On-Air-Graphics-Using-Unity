﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using OdinSerializer;

namespace Braver.Domain
{
    /// <summary>
    /// Item Domain model item.
    /// </summary>
    [System.Serializable]
    public class Item : IComparable<Item>
    {
        [Header("General Options")]
        [SerializeField]
        public string name;

        [SerializeField]
        public Vector3 position = Vector3.zero;
        [SerializeField]
        public Vector3 rotation = Vector3.zero;
        [SerializeField]
        public Vector3 scale = Vector3.one;

        [Header("Animation Options")]
        [SerializeField]
        private List<ItemAnimation> itemAnimations;

        [SerializeField]
        public int globalOrder;

        public static int GLOBAL_ORDER_TOTAL = 0;

        [SerializeField]
        public Item fatherItem = null;

        [SerializeField]
        private List<Item> childrenItems;

        public Item()
        {
            childrenItems = new List<Item>();
            itemAnimations = new List<ItemAnimation>();
            globalOrder = GLOBAL_ORDER_TOTAL;
            GLOBAL_ORDER_TOTAL++;
        }
        /// <summary>
        /// Verifies if the item has children items.
        /// </summary>
        /// <returns>True if yes, false if not.</returns>
        public bool HasChildrenItems()
        {
            return childrenItems.Count > 0;
        }
        /// <summary>
        /// Obtains a list of all children items in this item.
        /// </summary>
        /// <returns>List of child items</returns>
        public List<Item> ObtainChildrenItems()
        {
            return childrenItems;
        }

        public void OrderChildItems()
        {
            childrenItems.Sort();
            foreach (var childrenItem in childrenItems)
            {
                childrenItem.OrderChildItems();
            }
        }

        public bool RemoveItem(Item itemToRemove)
        {
            if (childrenItems.Remove(itemToRemove))
            {
                return true;
            }
            else
            {
                foreach (var childItem in childrenItems)
                {
                    if (childItem.HasChildrenItems())
                    {
                        return childItem.RemoveItem(itemToRemove);
                    }
                }
                return false;
            }
        }

        public void AddChildrenItem(Item item)
        {

            if (HasChildrenItems())
            {
                item.globalOrder = childrenItems.Last().globalOrder + 1;
            }
            else
            {
                item.globalOrder = this.globalOrder + 1;
            }
            item.fatherItem = this;

            childrenItems.Add(item);
        }

        public int CompareTo(Item other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            return globalOrder.CompareTo(other.globalOrder);
        }
        /// <summary>
        /// Increases all the orders of the items.
        /// </summary>
        /// <param name="itemHovered">The starting item to increase localOrder.</param>
        /// <param name="startingItem">Include the item passed through parameter in increasing its localOrder.</param>
        public bool IncreaseOrderItem(Item itemHovered,bool startingItem)
        {
            bool findItem = false;
            foreach (var childrenItem in childrenItems)
            {
                if (!findItem)
                {
                    findItem = childrenItem.Equals(itemHovered);
                    if (!findItem && childrenItem.HasChildrenItems())
                    {
                        findItem = childrenItem.IncreaseOrderItem(itemHovered, startingItem);
                    }

                    if (findItem && startingItem)
                    {
                        childrenItem.globalOrder++;
                    }
                }
                else
                {
                    childrenItem.globalOrder++;
                }
            }

            return findItem;
        }

        protected bool Equals(Item other)
        {
            return string.Equals(name, other.name) && globalOrder == other.globalOrder;
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Item) obj);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            unchecked
            {
                return ((name != null ? name.GetHashCode() : 0) * 397) ^ globalOrder;
            }
        }

        public bool HasAnimations()
        {
            return itemAnimations.Count > 0;
        }

        public List<ItemAnimation> ObtainAnimations()
        {
            return itemAnimations;
        }

        public ItemAnimation CreateNewItemAnimation(AnimationTemplate animTemplate)
        {
            ItemAnimation newAnim = new ItemAnimation(animTemplate);
            itemAnimations.Add(newAnim);
            return newAnim;
        }

        public void RemoveItemAnimation(ItemAnimation itemAnimation)
        {
            itemAnimations.Remove(itemAnimation);
        }
    }
}

