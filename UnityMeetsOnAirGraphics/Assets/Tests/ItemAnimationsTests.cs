﻿using System.Collections;
using System.Collections.Generic;
using Braver.Domain;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class ItemAnimationTests
    {
        [Test]
        public void DefaultValuesOfAnimationTemplate()
        {
            AnimationTemplate animTemplate = ScriptableObject.CreateInstance<AnimationTemplate>();

            Assert.AreEqual(0,animTemplate.ObtainDefaultDuration());
            Assert.AreEqual(Vector3.zero,animTemplate.ObtainDefaultValue());
            Assert.IsTrue(animTemplate.ObtainFromCurrentValue());
        }

        [Test]
        public void ItemAnimationCreatedFromTemplate()
        {

            AnimationTemplate animTemplate = ScriptableObject.CreateInstance<AnimationTemplate>();
            animTemplate.animationParameter = AnimationParameters.Position;
            animTemplate.defaultValues = new AnimationRuntime();
            Vector3 defValue = new Vector3(1, 2, 3);
            animTemplate.defaultValues.value = defValue;

            var itemAnim = new ItemAnimation(animTemplate);

            Assert.IsTrue(itemAnim.IsTypeAnimationParameter(animTemplate.animationParameter));
            Assert.IsTrue(itemAnim.IsFromCurrentPosition());
            Assert.AreEqual(0, itemAnim.ObtainDuration());
            Assert.AreEqual(defValue, itemAnim.ObtainValue());
        }

    }
}
