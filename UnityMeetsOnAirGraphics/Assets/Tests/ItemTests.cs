﻿using System.Collections;
using System.Collections.Generic;
using Braver.Domain;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class ItemTests
    {

        [SetUp]
        public void Setup()
        {
            Item.GLOBAL_ORDER_TOTAL = 0;
        }

        [Test]
        public void ItemDoesNotHaveChildrenItems()
        {
            Item item = ItemFactory.CreateEmptyItem();

            Assert.False(item.HasChildrenItems());
        }

        [Test]
        public void ItemIsCreatedWithAnEmptyList()
        {
            Item item = ItemFactory.CreateEmptyItem();
            Assert.AreEqual(0,item.ObtainChildrenItems().Count);
        }

        [Test]
        public void DefaultItemIsCreatedAndSetupThroughFactory()
        {
            Item item = ItemFactory.CreateEmptyItem();
            Assert.IsInstanceOf<Item>(item);
            Assert.IsNotNull(item);
            Assert.AreEqual("DefaultItem-",item.name);
        }

        [Test]
        public void ItemsAreEqualSameNameAndGlobalOrder()
        {
            Item item = ItemFactory.CreateEmptyItem();
            Item item2 = ItemFactory.CreateEmptyItem();

            item.globalOrder = 1;
            item2.globalOrder = 1;
            item.name = "item";
            item2.name = "item";

            Assert.AreEqual(item,item2);

            Item item3 = ItemFactory.CreateEmptyItem();

            Assert.AreNotEqual(item,item3);
        }

        #region RemoveItem
        [Test]
        public void RemoveChildItem()
        {
            Item item1 = ItemFactory.CreateEmptyItem();
            Item item11 = ItemFactory.CreateEmptyItem();
            Item item12 = ItemFactory.CreateEmptyItem();
            Item item13 = ItemFactory.CreateEmptyItem();

            item1.AddChildrenItem(item11);
            item1.AddChildrenItem(item13);
            item1.AddChildrenItem(item12);

            bool removed = item1.RemoveItem(item13);
            Assert.IsTrue(removed);
            Assert.IsFalse(item1.ObtainChildrenItems().Contains(item13));
        }
        [Test]
        public void RemoveChildItemOfaChildItem()
        {
            Item item1 = ItemFactory.CreateEmptyItem();
            Item item11 = ItemFactory.CreateEmptyItem();
            Item item112 = ItemFactory.CreateEmptyItem();
            Item item113 = ItemFactory.CreateEmptyItem();

            item1.AddChildrenItem(item11);
            item11.AddChildrenItem(item112);
            item11.AddChildrenItem(item113);

            bool removed = item1.RemoveItem(item113);
            Assert.IsTrue(removed);
            Assert.IsFalse(item11.ObtainChildrenItems().Contains(item113));
        }
        #endregion

        #region SortItems
        [Test]
        public void OnAddChildItemsAreSortedByItemOrderNumberAscWithoutChildrenItems()
        {
            Item item1 = ItemFactory.CreateEmptyItem();
            Item item11 = ItemFactory.CreateEmptyItem();
            Item item12 = ItemFactory.CreateEmptyItem();
            Item item13 = ItemFactory.CreateEmptyItem();

            item1.AddChildrenItem(item11);
            item1.AddChildrenItem(item13);
            item1.AddChildrenItem(item12);

            int startOrder = 1;

            foreach (var sceneItem in item1.ObtainChildrenItems())
            {
                Assert.AreEqual(startOrder, sceneItem.globalOrder);
                startOrder++;
            }
        }

        [Test]
        public void OnAddChildItemsAreSortedByItemOrderNumberAscWithChildItems()
        {
            Item item1 = ItemFactory.CreateEmptyItem();
            Item item11 = ItemFactory.CreateEmptyItem();
            Item item112 = ItemFactory.CreateEmptyItem();
            Item item113 = ItemFactory.CreateEmptyItem();

            item1.AddChildrenItem(item11);
            item11.AddChildrenItem(item112);
            item11.AddChildrenItem(item113);

            int startOrder = 1;

            foreach (var sceneItem in item1.ObtainChildrenItems())
            {
                startOrder = RecursiveTestSortChildrenItems(sceneItem, startOrder);
            }
        }

        public int RecursiveTestSortChildrenItems(Item itemToTest, int startOrder)
        {
            Assert.AreEqual(startOrder, itemToTest.globalOrder);
            startOrder++;

            if (itemToTest.HasChildrenItems())
            {
                foreach (var childrenItem in itemToTest.ObtainChildrenItems())
                {
                    startOrder = RecursiveTestSortChildrenItems(childrenItem, startOrder);
                }
            }
            return startOrder;
        }

        [Test]
        public void ChildItemsAreSortedByItemOrderNumberAscWithoutChildrenItems()
        {
            Item item1 = ItemFactory.CreateEmptyItem();
            Item item11 = ItemFactory.CreateEmptyItem();
            Item item12 = ItemFactory.CreateEmptyItem();
            Item item13 = ItemFactory.CreateEmptyItem();

            item1.AddChildrenItem(item11);
            item1.AddChildrenItem(item12);
            item1.AddChildrenItem(item13);

            item13.globalOrder = 2;
            item12.globalOrder = 3;

            item1.OrderChildItems();

            int startOrder = 1;

            foreach (var sceneItem in item1.ObtainChildrenItems())
            {
                Assert.AreEqual(startOrder, sceneItem.globalOrder);
                startOrder++;
            }
        }

        [Test]
        public void ChildItemsAreSortedByItemOrderNumberAscWithChildItems()
        {
            Item item1 = ItemFactory.CreateEmptyItem();
            Item item11 = ItemFactory.CreateEmptyItem();
            Item item112 = ItemFactory.CreateEmptyItem();
            Item item113 = ItemFactory.CreateEmptyItem();

            item1.AddChildrenItem(item11);
            item11.AddChildrenItem(item112);
            item11.AddChildrenItem(item113);

            item113.globalOrder = 2;
            item112.globalOrder = 3;

            item1.OrderChildItems();

            int startOrder = 1;

            foreach (var sceneItem in item1.ObtainChildrenItems())
            {
                startOrder = RecursiveTestSortChildrenItems(sceneItem, startOrder);
            }
        }
        #endregion

        #region Animations

        [Test]
        public void ItemHasAnimations()
        {
            Item item = ItemFactory.CreateEmptyItem();

            Assert.IsFalse(item.HasAnimations());

            AnimationTemplate animTemplate = ScriptableObject.CreateInstance<AnimationTemplate>();
            animTemplate.animationParameter = AnimationParameters.Position;
            animTemplate.defaultValues = new AnimationRuntime();

            item.CreateNewItemAnimation(animTemplate);

            Assert.IsTrue(item.HasAnimations());
        }

        [Test]
        public void ItemAddsAnimationBasedOnTemplate()
        {
            Item item = ItemFactory.CreateEmptyItem();

            AnimationTemplate animTemplate = ScriptableObject.CreateInstance<AnimationTemplate>();
            animTemplate.animationParameter = AnimationParameters.Position;
            animTemplate.defaultValues = new AnimationRuntime();
            Vector3 defValue = new Vector3(1, 2, 3);
            animTemplate.defaultValues.value = defValue;

            item.CreateNewItemAnimation(animTemplate);

            var itemAnim = item.ObtainAnimations()[0];
            Assert.IsTrue(itemAnim.IsTypeAnimationParameter(animTemplate.animationParameter));
            Assert.IsTrue(itemAnim.IsFromCurrentPosition());
            Assert.AreEqual(0,itemAnim.ObtainDuration());
            Assert.AreEqual(defValue, itemAnim.ObtainValue());
        }

        #endregion


        #region ImageItem

        [Test]
        public void ImageItemIsCreatedAndSetupThroughFactory()
        {
            string path = "C://path";
            Item item = ItemFactory.CreateImageItem(path);
            Assert.IsInstanceOf<ImageItem>(item);
            Assert.IsNotNull(item);
            Assert.AreEqual(path, ((ImageItem) item).imagePath);
        }
        #endregion
    }
}
