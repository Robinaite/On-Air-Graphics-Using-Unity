﻿using System.Collections;
using System.Collections.Generic;
using Braver.Domain;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class ProjectTests
    {

        [Test]
        public void OnProjectCreationAddEmptySceneAndSetActive()
        {
            Project project = new Project();

            Assert.AreEqual(1,project.ObtainSceneList().Count);
            Assert.AreEqual(project.ObtainCurrentActiveScene(),project.ObtainSceneList()[0]);
        }

        [Test]
        public void NewSceneIsCreatedAndAddedToProject()
        {
            Project project = new Project();


            Scene scene1 = project.CreateNewScene(false);

            Assert.Contains(scene1, project.ObtainSceneList());
            Assert.AreEqual(scene1,project.ObtainSceneList()[1]);

            Scene scene2 = project.CreateNewScene(true);

            Assert.Contains(scene2, project.ObtainSceneList());
            Assert.AreEqual(scene2, project.ObtainSceneList()[2]);
        }

        [Test]
        public void SceneIsSetAsActive()
        {
            Project project = new Project();
            Scene sceneToSetActive = new Scene();

            project.SetActiveScene(sceneToSetActive);

            Assert.AreNotEqual(sceneToSetActive, project.ObtainCurrentActiveScene());

            Scene sceneNotActive = project.CreateNewScene(false);

            Assert.AreNotEqual(sceneNotActive, project.ObtainCurrentActiveScene());

            project.SetActiveScene(sceneNotActive);

            Assert.AreEqual(sceneNotActive, project.ObtainCurrentActiveScene());

            Scene sceneActive = project.CreateNewScene(true);

            Assert.AreEqual(sceneActive, project.ObtainCurrentActiveScene());
        }

        [Test]
        public void AddDefaultItemToCurrentActiveScene()
        {
            Project project = new Project();
            Item item = ItemFactory.CreateEmptyItem();

            project.AddItemToCurrentScene(item);

            Assert.Contains(item,project.ObtainCurrentActiveScene().ObtainSceneItems());
        }
        [Test]
        public void AddImageItemToCurrentActiveScene()
        {
            Project project = new Project();
            ImageItem item = ItemFactory.CreateImageItem("C://path");

            project.AddItemToCurrentScene(item);

            Assert.Contains(item, project.ObtainCurrentActiveScene().ObtainSceneItems());
        }
    }
}
