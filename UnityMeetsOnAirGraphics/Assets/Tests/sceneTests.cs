﻿using System.Collections;
using System.Collections.Generic;
using Braver.Domain;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class SceneTests
    {
        [SetUp]
        public void Setup()
        {
            Item.GLOBAL_ORDER_TOTAL = 0;
        }

        [Test]
        public void ScenesAreNotEqual()
        {
            Scene scene1 = new Scene();
            Scene scene2 = new Scene();

            Assert.AreNotEqual(scene1,scene2);
        }
        #region SortItems
        [Test]
        public void SceneItemsAreSortedByItemOrderNumberAscWithoutChildrenItems()
        {
            Scene scene1 = new Scene();
            Item item1 = ItemFactory.CreateEmptyItem();
            Item item2 = ItemFactory.CreateEmptyItem();
            ImageItem item3 = new ImageItem();

            scene1.AddSceneItem(item3);
            scene1.AddSceneItem(item2);
            scene1.AddSceneItem(item1);

            int startOrder = 0;

            foreach (var sceneItem in scene1.ObtainSceneItems())
            {
                   Assert.AreEqual(startOrder,sceneItem.globalOrder);
                   startOrder++;
            }
        }
        [Test]
        public void SceneItemsAreSortedByItemOrderNumberAscWithChildrenItems()
        {
            Scene scene1 = new Scene();
            Item item1 = ItemFactory.CreateEmptyItem();
            Item item11 = ItemFactory.CreateEmptyItem();
            Item item12 = ItemFactory.CreateEmptyItem();
            Item item13 = ItemFactory.CreateEmptyItem();

            Item item2 = ItemFactory.CreateEmptyItem();
            ImageItem item3 = new ImageItem();

            item1.AddChildrenItem(item11);
            item1.AddChildrenItem(item13);
            item1.AddChildrenItem(item12);

            scene1.AddSceneItem(item3);
            scene1.AddSceneItem(item2);
            scene1.AddSceneItem(item1);

            int startOrder = 0;

            foreach (var sceneItem in scene1.ObtainSceneItems())
            {
                startOrder = RecursiveTestSortChildrenItems(sceneItem, startOrder);
            }
        }

        public int RecursiveTestSortChildrenItems(Item itemToTest,int startOrder)
        {
            Assert.AreEqual(startOrder, itemToTest.globalOrder);
            startOrder++;
            
            if (itemToTest.HasChildrenItems())
            {
                foreach (var childrenItem in itemToTest.ObtainChildrenItems())
                {
                    startOrder = RecursiveTestSortChildrenItems(childrenItem, startOrder);
                }
            }
            return startOrder;
        }
#endregion

        #region IncreaseItemOrder
        [Test]
        public void IncreaseItemOrderIncludingCurrentItemWithoutChildrenItems()
        {
            Scene scene1 = new Scene();
            Item item1 = ItemFactory.CreateEmptyItem();
            Item item2 = ItemFactory.CreateEmptyItem();
            ImageItem item3 = new ImageItem();

            scene1.AddSceneItem(item1);
            scene1.AddSceneItem(item2);
            scene1.AddSceneItem(item3);
            
            scene1.IncreaseOrderItem(item2,true);

            int startOrder = 0;

            foreach (var sceneItem in scene1.ObtainSceneItems())
            {
                if (sceneItem.Equals(item2))
                {
                    startOrder++;
                }
                Assert.AreEqual(startOrder, sceneItem.globalOrder);
                startOrder++;
            }
        }
        [Test]
        public void IncreaseItemOrderExcludingCurrentItemWithoutChildrenItems()
        {
            Scene scene1 = new Scene();
            Item item1 = ItemFactory.CreateEmptyItem();
            Item item2 = ItemFactory.CreateEmptyItem();
            ImageItem item3 = new ImageItem();

            scene1.AddSceneItem(item1);
            scene1.AddSceneItem(item2);
            scene1.AddSceneItem(item3);

            scene1.IncreaseOrderItem(item2, false);

            int startOrder = 0;

            foreach (var sceneItem in scene1.ObtainSceneItems())
            {
                Assert.AreEqual(startOrder, sceneItem.globalOrder);
                startOrder++;
                if (sceneItem.Equals(item2))
                {
                    startOrder++;
                }
            }
        }

        [Test]
        public void IncreaseItemOrderOfChildItemIncludingCurrentItem()
        {
            Scene scene1 = new Scene();
            Item item1 = ItemFactory.CreateEmptyItem();
            Item item11 = ItemFactory.CreateEmptyItem();
            Item item12 = ItemFactory.CreateEmptyItem();
            Item item13 = ItemFactory.CreateEmptyItem();

            Item item2 = ItemFactory.CreateEmptyItem();
            ImageItem item3 = new ImageItem();

            item1.AddChildrenItem(item11);
            item1.AddChildrenItem(item12);
            item1.AddChildrenItem(item13);

            scene1.AddSceneItem(item1);
            scene1.AddSceneItem(item2);
            scene1.AddSceneItem(item3);

            scene1.IncreaseOrderItem(item12, true);

            int startOrder = 0;

            foreach (var sceneItem in scene1.ObtainSceneItems())
            {
                startOrder = RecursiveTestIncreaseOrderChildrenItemsIncludingItem(sceneItem, startOrder, item12);
            }
        }

        public int RecursiveTestIncreaseOrderChildrenItemsIncludingItem(Item itemToTest, int startOrder,Item childItemToCheckAgainst)
        {
            if (itemToTest.Equals(childItemToCheckAgainst))
            {
                startOrder++;
            }
            Assert.AreEqual(startOrder, itemToTest.globalOrder,"Only run one of the Increase Order or Sort at a time. Due to static variable reset.");
            startOrder++;

            if (itemToTest.HasChildrenItems())
            {
                foreach (var childrenItem in itemToTest.ObtainChildrenItems())
                {
                    startOrder = RecursiveTestIncreaseOrderChildrenItemsIncludingItem(childrenItem, startOrder,childItemToCheckAgainst);
                }
            }
            return startOrder;
        }

        [Test]
        public void IncreaseItemOrderOfChildItemExcludingCurrentItem()
        {
            Scene scene1 = new Scene();
            Item item1 = ItemFactory.CreateEmptyItem();
            Item item11 = ItemFactory.CreateEmptyItem();
            Item item12 = ItemFactory.CreateEmptyItem();
            Item item13 = ItemFactory.CreateEmptyItem();

            Item item2 = ItemFactory.CreateEmptyItem();
            ImageItem item3 = new ImageItem();

            item1.AddChildrenItem(item11);
            item1.AddChildrenItem(item12);
            item1.AddChildrenItem(item13);

            scene1.AddSceneItem(item1);
            scene1.AddSceneItem(item2);
            scene1.AddSceneItem(item3);

            scene1.IncreaseOrderItem(item12, false);

            int startOrder = 0;

            foreach (var sceneItem in scene1.ObtainSceneItems())
            {
                startOrder = RecursiveTestIncreaseOrderChildrenItemsExcludingItem(sceneItem, startOrder, item12);
            }
        }

        public int RecursiveTestIncreaseOrderChildrenItemsExcludingItem(Item itemToTest, int startOrder, Item childItemToCheckAgainst)
        {
            
            Assert.AreEqual(startOrder, itemToTest.globalOrder);
            startOrder++;
            if (itemToTest.Equals(childItemToCheckAgainst))
            {
                startOrder++;
            }
            if (itemToTest.HasChildrenItems())
            {
                foreach (var childrenItem in itemToTest.ObtainChildrenItems())
                {
                    startOrder = RecursiveTestIncreaseOrderChildrenItemsExcludingItem(childrenItem, startOrder, childItemToCheckAgainst);
                }
            }
            return startOrder;
        }
#endregion

        [Test]
        public void RemoveItemFromSceneWithouthChildren()
        {
            Scene scene1 = new Scene();
            Item item1 = ItemFactory.CreateEmptyItem();
            Item item2 = ItemFactory.CreateEmptyItem();
            ImageItem item3 = new ImageItem();

            scene1.AddSceneItem(item3);
            scene1.AddSceneItem(item2);
            scene1.AddSceneItem(item1);

            bool removed = scene1.RemoveItem(item2);
            Assert.IsTrue(removed);
            Assert.IsFalse(scene1.ObtainSceneItems().Contains(item2));
        }
        [Test]
        public void RemoveAChildItemFromScene()
        {
            Scene scene1 = new Scene();
            Item item1 = ItemFactory.CreateEmptyItem();
            Item item11 = ItemFactory.CreateEmptyItem();
            Item item12 = ItemFactory.CreateEmptyItem();
            Item item13 = ItemFactory.CreateEmptyItem();

            Item item2 = ItemFactory.CreateEmptyItem();
            ImageItem item3 = new ImageItem();

            item1.AddChildrenItem(item11);
            item1.AddChildrenItem(item13);
            item1.AddChildrenItem(item12);

            scene1.AddSceneItem(item3);
            scene1.AddSceneItem(item2);
            scene1.AddSceneItem(item1);

            bool removed = scene1.RemoveItem(item13);
            Assert.IsTrue(removed);
            Assert.IsFalse(item1.ObtainChildrenItems().Contains(item13));
        }


        #region Default Items
        [Test]
        public void AddOneDefaultItemToScene()
        {
            Scene scene = new Scene();
            Item itemCreated = ItemFactory.CreateEmptyItem();

           scene.AddSceneItem(itemCreated);

           Assert.AreEqual(1, scene.ObtainSceneItems().Count);

           Assert.AreEqual(itemCreated, scene.ObtainSceneItems()[0]);
        }

        [Test]
        public void AddTwoDefaultItemToScene()
        {
            Scene scene = new Scene();
            Item itemCreated1 = ItemFactory.CreateEmptyItem();
            Item itemCreated2 = ItemFactory.CreateEmptyItem();

            scene.AddSceneItem(itemCreated1);
            scene.AddSceneItem(itemCreated2);

            Assert.AreEqual(2, scene.ObtainSceneItems().Count);

            Assert.AreEqual(itemCreated1, scene.ObtainSceneItems()[0]);
            Assert.AreEqual(itemCreated2, scene.ObtainSceneItems()[1]);
        }
        #endregion

        #region Image Item
        [Test]
        public void AddOneImageItemToScene()
        {
            Scene scene = new Scene();
            ImageItem itemCreated = new ImageItem();

            scene.AddSceneItem(itemCreated);

            Assert.AreEqual(1, scene.ObtainSceneItems().Count);
            Assert.IsInstanceOf(typeof(ImageItem), scene.ObtainSceneItems()[0]);

            Assert.AreEqual(itemCreated, scene.ObtainSceneItems()[0]);
        }
        [Test]
        public void AddTwoImageItemToScene()
        {
            Scene scene = new Scene();
            ImageItem itemCreated1 = new ImageItem();
            ImageItem itemCreated2 = new ImageItem();

            scene.AddSceneItem(itemCreated1);
            scene.AddSceneItem(itemCreated2);

            Assert.AreEqual(2, scene.ObtainSceneItems().Count);
            Assert.IsInstanceOf(typeof(ImageItem), scene.ObtainSceneItems()[0]);
            Assert.IsInstanceOf(typeof(ImageItem), scene.ObtainSceneItems()[1]);

            Assert.AreEqual(itemCreated1, scene.ObtainSceneItems()[0]);
            Assert.AreEqual(itemCreated2, scene.ObtainSceneItems()[1]);
        }
        #endregion

        
    }
}
