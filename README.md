# On-Air Graphics Creation Platform using Unity for Real-time Rendering

This project was developed for a [Univeristy](http://isep.ipp.pt/) Internship done at GOATPixel ([Braver media Group](http://braver.pt/)). The objective was to create a prototype of how we could use real-time rendering techniques for on-air graphics, with a focus on the rising needs for **Esports** live broadcasts. The idea was to use Unity as a base, due to it's powerful real-time rendering engine and interactive aspects.

For more about the internship you can read the internship report: [Internship Report](RobinCouwenberg-Internship-Report.pdf)

In the internship report you can find the following sections:
1. Introduction to On-Air Graphics
2. State of the Art
3. Analysis & Design of the solution
4. Implementation
5. Conclusions & Limitations
6. References

## Project Feature Demonstration Video

![Demonstration Video](DemonstrationVideo.mov)

## Abstract

Digital content is getting increasingly more important as it is where the new generation of people are getting their information and entertainment from. There is especially a growth in live content creation and sharing, as it is easy to make while interacting directly with the target market.

These live contents, more often called Live Broadcasts, need On-Air graphics, also called Overlays, to help explain what is happening and to increase the quality of the broadcast itself.

There is, in the creation of On-Air Graphics, a huge gap between high-end products and low-end products. This gap constitutes especially of features on the creation of dynamic and animated On-Air Graphics in combination with the cost of usage of the respective products.

The product created makes use of pre-existing technologies as much as possible, as to lower the development costs. These technologies give the necessary tools to create animated and dynamic On-Air Graphics with the same quality as the ones created by high-end products.

The technology chosen for the development of this product is Unity. As Unity is primarily a game engine, its features will allow the easy development of a product that will be able to create animated and dynamic On-Air Graphics. The reason for selecting a game engine is that games require high quality levels of animation and dynamicity, which are the same requirements for creating great On-Air Graphics.

The product developed showcases that there is an increasingly need for these kinds of products that make use of real-time rendering techniques. As the fidelity and performance of real-time rendering techniques increases, just so will the animated and dynamic On-Air Graphics.


## Project information:

The project was developed using Unity 2018.3.12f.

## Project Features:

1. Multiple projects
2. Multiple Scenes
3. Multiple overlay items per scene
4. Multiple animations based on rules per overlay item
5. Drag & Drop re-ordering and childing
6. Animate position, rotation and scale in 3D space.
7. Animation execution based on rules (Such as changing scenes,manually , waiting for other animations, etc...)
8. Overlay output to NDI or Spout


**This project is an early prototype, not suggested for commercial use!**

## 3rd Party Assets Licenses

DOTween is licensed under their own proprietary licensed found here: [DOTween License](http://dotween.demigiant.com/license.php)  
KlakNDI is licensed under MIT-Style license, read more about the specific license here: [KlakNDI License](https://github.com/keijiro/KlakNDI/blob/master/LICENSE)  
KlakSpout is licensed under MIT-Style license, read more about the specific license here: [KlakSpout License](https://github.com/keijiro/KlakSpout/blob/master/LICENSE.md)  
